package gui;

import java.util.Random;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.swt.SWT;

import org.eclipse.swt.widgets.Group;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

import client.*;

import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;

import common.CCard;
import common.CMessage29.GameState29;
import common.CMessage29.NextJobTypes;

import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;



public class FormMain {

	protected Display display;
	protected Shell shlPlay;
	
	
	private Text tbServer;
	private Text tbPort;
	private Text tbName;
	private Button btngroup_West;
	private Button btngroup_East;
	private Button btngroup_North;
	private Button btngroup_South;
	private Label tbgroup_West2;
	private Label tbgroup_East2;
	private Label tbgroup_North2;
	private Label tbgroup_South2;
	private Group group_West;
	private Group group_East;
	private Group group_North;
	private Group group_South;
	private Button btnConnect;
	private Button btnReady;
	private Button btnPass;
	private Button btnBid;
	private Combo comboBid;
	private Group group_cards;
	private	Group group_Bid;
	
	private Button arr_btngroup_Player[];
	private Button arr_btnPlayerInfo[];
	private Label arr_tbgroup_Player[];
	private Group arr_group_Player[];
	private Button arr_btn_Cards[];
	private Label arr_lblLastHands[];
	public UIDataThread o_uidata;
	
	C29Client client;
	String sDefaultServer;
	String sDefaultPort;
	String sDefaultName;
	private Button btnTrump;
	private Button btnSetTrump;
	private Group group_info;
	private Group group_BidResult;
	private Text tb_bidWinner;
	private Label label_PairName;
	private Label lblPairShown;
	private Label label_DoubleName;
	private Label lblDouble;
	private Label label_ReDoubleName;
	private Label lblReDouble;
	private Label label_SingleHand;
	private Label lblSingleHandName;
	private Button btnCardColor;
	private Group groupCardColor;
	private TabItem tbtmChat;
	private Group group_chat;
	private Text tbChatOutput;
	private Text tbChatInput;
	private Button btnChatSend;
	private Button buttonReady23;
	private Button buttonDouble;
	private Button buttonRe;
	private Button buttonSingle;
	private Group group_DoubleRe;
	private Group group_lasthand;
	private Label lbl_LastHandSouth;
	private Label lbl_LastHandNorth;
	private Label lbl_LastHandEast;
	private Label lbl_LastHandWest;
	private Group group_SouthInfo;
	private Group group_EastInfo;
	private Group group_NorthInfo;
	private Group group_WestInfo;
	private Button btnSouthInfo;
	private Button btnEastInfo;
	private Button btnNorthInfo;
	private Button btnWestInfo;
	private Group group_score;
	private Label lblTeam;
	private Label lblSet;
	private Label lblPoint;
	private Label lblEvenTeam;
	private Label lblEvenTeamSet;
	private Label lblEvenTeamPoint;
	private Label lblOddTeam;
	private Label lblOddTeamSet;
	private Label lblOddTeamPoint;
	private Button btnShowPair;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args){
		try {
			FormMain window = new FormMain();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public FormMain(){
		client = null; 
		arr_btngroup_Player = new Button[4];
		arr_tbgroup_Player = new Label[4];
		arr_group_Player = new Group[4];
		arr_btn_Cards	 = new Button[8];
		arr_lblLastHands = new Label[4];
		arr_btnPlayerInfo = new Button[4];
		o_uidata = new UIDataThread(this);
		sDefaultServer = "localhost";
		sDefaultPort = "6666";
		Random rand = new Random();
		sDefaultName = "dizzling"+ Integer.toString(rand.nextInt(100));
		
	}
	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		shlPlay = new Shell(display,SWT.CLOSE | SWT.TITLE | SWT.MIN);
		createContents();
		ContentCreationDone();
		
		shlPlay.open();
		shlPlay.layout();
		while (!shlPlay.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		//shlPlay = new Shell();
		shlPlay.setSize(684, 536);
		shlPlay.setText("Play 29");
		shlPlay.setLayout(new FormLayout());
		
		Group group_Play = new Group(shlPlay, SWT.NONE);
		FormData fd_group_Play = new FormData();
		fd_group_Play.bottom = new FormAttachment(0, 325);
		fd_group_Play.right = new FormAttachment(0, 492);
		fd_group_Play.left = new FormAttachment(0, 10);
		group_Play.setLayoutData(fd_group_Play);
		group_Play.setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		group_Play.setLayout(new FormLayout());
		
		group_West = new Group(group_Play, SWT.NONE);

		group_West.setLayout(new FormLayout());
		FormData fd_group_West = new FormData();
		fd_group_West.left = new FormAttachment(0,10);
		fd_group_West.top = new FormAttachment(30,0);
		fd_group_West.bottom = new FormAttachment(70,0);
		fd_group_West.right = new FormAttachment(20,10);
		group_West.setLayoutData(fd_group_West);
		
		btngroup_West = new Button(group_West, SWT.NONE);
		btngroup_West.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		btngroup_West.setAlignment(SWT.CENTER);
		FormData fd_lblgroup_West = new FormData();
		fd_lblgroup_West.top = new FormAttachment(0, 0);
		fd_lblgroup_West.left = new FormAttachment(0, 0);
		fd_lblgroup_West.right = new FormAttachment(100, 0);
		btngroup_West.setLayoutData(fd_lblgroup_West);
		btngroup_West.setText("Free");
		
		tbgroup_West2 = new Label(group_West, SWT.WRAP | SWT.CENTER);
		FormData fd_tbgroup_West2 = new FormData();
		fd_tbgroup_West2.top = new FormAttachment(btngroup_West, 0);
		fd_tbgroup_West2.left = new FormAttachment(0, 0);
		fd_tbgroup_West2.right = new FormAttachment(100, 0);
		fd_tbgroup_West2.bottom = new FormAttachment(100, 0);
		tbgroup_West2.setLayoutData(fd_tbgroup_West2);
		tbgroup_West2.setText("");
		
		group_East = new Group(group_Play, SWT.NONE);
		group_East.setLayout(new FormLayout());
		FormData fd_group_East = new FormData();
		fd_group_East.left = new FormAttachment(80,-10);
		fd_group_East.top = new FormAttachment(30, 0);
		fd_group_East.bottom = new FormAttachment(70,0);
		fd_group_East.right = new FormAttachment(100,-10);
		group_East.setLayoutData(fd_group_East);
		
		btngroup_East = new Button(group_East, SWT.NONE);
		btngroup_East.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		btngroup_East.setAlignment(SWT.CENTER);
		FormData fd_lblgroup_East = new FormData();
		fd_lblgroup_East.top = new FormAttachment(0, 0);
		fd_lblgroup_East.left = new FormAttachment(0, 0);
		fd_lblgroup_East.right = new FormAttachment(100, 0);
		btngroup_East.setLayoutData(fd_lblgroup_East);
		btngroup_East.setText("Free");
		
		tbgroup_East2 = new Label(group_East, SWT.WRAP | SWT.CENTER);
		FormData fd_tbgroup_East2 = new FormData();
		fd_tbgroup_East2.top = new FormAttachment(btngroup_East, 0);
		fd_tbgroup_East2.left= new FormAttachment(0, 0);
		fd_tbgroup_East2.right = new FormAttachment(100, 0);
		fd_tbgroup_East2.bottom = new FormAttachment(100, 0);
		tbgroup_East2.setLayoutData(fd_tbgroup_East2);
		tbgroup_East2.setText("");
		
		group_North = new Group(group_Play, SWT.NONE);
		group_North.setLayout(new FormLayout());
		FormData fd_group_North = new FormData();
		fd_group_North.left = new FormAttachment(40,0);
		fd_group_North.top = new FormAttachment(0, 10);
		fd_group_North.bottom = new FormAttachment(40,10);
		fd_group_North.right = new FormAttachment(60,0);
		group_North.setLayoutData(fd_group_North);
		
		btngroup_North = new Button(group_North, SWT.NONE);
		btngroup_North.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		btngroup_North.setAlignment(SWT.CENTER);
		FormData fd_lblgroup_North = new FormData();
		fd_lblgroup_North.top = new FormAttachment(0, 0);
		fd_lblgroup_North.left = new FormAttachment(0, 0);
		fd_lblgroup_North.right = new FormAttachment(100, 0);
		btngroup_North.setLayoutData(fd_lblgroup_North);
		btngroup_North.setText("Free");
		
		tbgroup_North2 = new Label(group_North, SWT.WRAP | SWT.CENTER);
		FormData fd_tbgroup_North2 = new FormData();
		fd_tbgroup_North2.top = new FormAttachment(btngroup_North, 0);
		fd_tbgroup_North2.left = new FormAttachment(0, 0);
		fd_tbgroup_North2.right = new FormAttachment(100, 0);
		fd_tbgroup_North2.bottom = new FormAttachment(100, 0);
		tbgroup_North2.setLayoutData(fd_tbgroup_North2);
		tbgroup_North2.setText("");
		
		group_South = new Group(group_Play, SWT.NONE);
		group_South.setLayout(new FormLayout());
		FormData fd_group_South = new FormData();
		fd_group_South.left = new FormAttachment(40,0);
		fd_group_South.top = new FormAttachment(60, -10);
		fd_group_South.bottom = new FormAttachment(100,-10);
		fd_group_South.right = new FormAttachment(60,0);
		group_South.setLayoutData(fd_group_South);
		
		btngroup_South = new Button(group_South, SWT.NONE);
		btngroup_South.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		btngroup_South.setAlignment(SWT.CENTER);
		FormData fd_lblgroup_South = new FormData();
		fd_lblgroup_South.top = new FormAttachment(0, 0);
		fd_lblgroup_South.left = new FormAttachment(0, 0);
		fd_lblgroup_South.right = new FormAttachment(100, 0);
		btngroup_South.setLayoutData(fd_lblgroup_South);
		btngroup_South.setText("Free");
		
		tbgroup_South2 = new Label(group_South, SWT.WRAP | SWT.CENTER);
		FormData fd_tbgroup_South2 = new FormData();
		fd_tbgroup_South2.top = new FormAttachment(btngroup_South, 0);
		fd_tbgroup_South2.left = new FormAttachment(0, 0);
		fd_tbgroup_South2.right = new FormAttachment(100, 0);
		fd_tbgroup_South2.bottom = new FormAttachment(100, 0);
		tbgroup_South2.setLayoutData(fd_tbgroup_South2);
		tbgroup_South2.setText("");		
		
		Group group_Connection = new Group(shlPlay, SWT.NONE);
		fd_group_Play.top = new FormAttachment(group_Connection, 0, SWT.TOP);
		group_Connection.setLayout(new GridLayout(3, true));
		FormData fd_group_Connection = new FormData();
		fd_group_Connection.bottom = new FormAttachment(0, 155);
		fd_group_Connection.right = new FormAttachment(100, -10);
		fd_group_Connection.top = new FormAttachment(0, 20);
		fd_group_Connection.left = new FormAttachment(0, 498);
		group_Connection.setLayoutData(fd_group_Connection);
		
		Label lblName = new Label(group_Connection, SWT.NONE);
		lblName.setText("Name");
		
		tbName = new Text(group_Connection, SWT.BORDER);
		tbName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		tbName.setText(sDefaultName);
		
		Label lblServer = new Label(group_Connection, SWT.NONE);
		lblServer.setText("Server");
		new Label(group_Connection, SWT.NONE);
		
		Label lblPort = new Label(group_Connection, SWT.NONE);
		lblPort.setText("Port");
		
		tbServer = new Text(group_Connection, SWT.BORDER);
		tbServer.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		tbServer.setText(sDefaultServer);
		
		tbPort = new Text(group_Connection, SWT.BORDER);
		tbPort.setText(sDefaultPort);
		
		btnConnect = new Button(group_Connection, SWT.NONE);
		btnConnect.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		btnConnect.setText("Connect");
		
		group_cards = new Group(shlPlay, SWT.NONE);
		FormData fd_group_cards = new FormData();
		fd_group_cards.top = new FormAttachment(group_Play, 75);
		fd_group_cards.right = new FormAttachment(group_Play, 0, SWT.RIGHT);
		fd_group_cards.bottom = new FormAttachment(100, -20);
		fd_group_cards.left = new FormAttachment(group_Play, 0, SWT.LEFT);
		group_cards.setLayoutData(fd_group_cards);
		
		group_Bid = new Group(shlPlay, SWT.NONE);
		FormData fd_group_Bid = new FormData();
		fd_group_Bid.bottom = new FormAttachment(group_Connection, 95, SWT.BOTTOM);
		fd_group_Bid.right = new FormAttachment(group_Connection, 0, SWT.RIGHT);
		fd_group_Bid.top = new FormAttachment(group_Connection, 6);
		fd_group_Bid.left = new FormAttachment(group_Play, 6);
		group_Bid.setLayoutData(fd_group_Bid);
		
		btnReady = new Button(group_Bid, SWT.NONE);
		btnReady.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnReadyClick();
			}
		});
		btnReady.setBounds(10, 10, 72, 29);
		btnReady.setText("Start");
		
		btnPass = new Button(group_Bid, SWT.NONE);
		btnPass.setText("Pass");
		btnPass.setBounds(10, 49, 72, 29);
		
		btnBid = new Button(group_Bid, SWT.NONE);
		btnBid.setText("Bid");
		btnBid.setBounds(93, 49, 72, 29);
		
		comboBid = new Combo(group_Bid, SWT.READ_ONLY);
		comboBid.setBounds(100, 12, 60, 29);
		
		Group group_Trump = new Group(shlPlay, SWT.NONE);
		group_Trump.setLayout(new FormLayout());
		FormData fd_group_Trump = new FormData();
		fd_group_Trump.bottom = new FormAttachment(group_Play, 0, SWT.BOTTOM);
		fd_group_Trump.right = new FormAttachment(group_Connection, 0, SWT.RIGHT);
		fd_group_Trump.top = new FormAttachment(group_Bid, 6);
		fd_group_Trump.left = new FormAttachment(group_Play, 6);
		group_Trump.setLayoutData(fd_group_Trump);
		
		groupCardColor = new Group(group_Trump,SWT.NONE);
		groupCardColor.setLayout(new FillLayout());
		FormData fd_groupCardColor = new FormData();
		fd_groupCardColor.bottom = new FormAttachment(100, -4);
		fd_groupCardColor.right = new FormAttachment(0, 52);
		fd_groupCardColor.top = new FormAttachment(0, 4);
		fd_groupCardColor.left = new FormAttachment(0, 8);
		groupCardColor.setLayoutData(fd_groupCardColor);
		
		btnCardColor = new Button(groupCardColor, SWT.PUSH);
		btnCardColor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TrumpButtonClick();
			}
		});
		
		btnTrump = new Button(group_Trump, SWT.NONE);
		btnTrump.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ShowTrumpButtonClick();
			}
		});
		FormData fd_btnTrump = new FormData();
		fd_btnTrump.bottom = new FormAttachment(50, -2);
		fd_btnTrump.right = new FormAttachment(0, 163);
		fd_btnTrump.top = new FormAttachment(0, 5);
		fd_btnTrump.left = new FormAttachment(0, 69);
		btnTrump.setLayoutData(fd_btnTrump);
		btnTrump.setText("Show Color");
		
		btnSetTrump = new Button(group_Trump, SWT.NONE);
		btnSetTrump.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SetTrumpButtonClick();
			}
		});
		FormData fd_btnSetTrump = new FormData();
		fd_btnSetTrump.bottom = new FormAttachment(100, -5);
		fd_btnSetTrump.right = new FormAttachment(0, 163);
		fd_btnSetTrump.top = new FormAttachment(50, 2);
		fd_btnSetTrump.left = new FormAttachment(0, 69);
		btnSetTrump.setLayoutData(fd_btnSetTrump);
		btnSetTrump.setText("Set Color");
		
		btnShowPair = new Button(group_Trump, SWT.NONE);
		btnShowPair.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ShowPairClick();
			}
		});
		btnShowPair.setText("Show Pair");
		FormData fd_btnShowPair = new FormData();
		fd_btnShowPair.right = new FormAttachment(btnSetTrump,0,SWT.RIGHT);
		fd_btnShowPair.left = new FormAttachment(btnSetTrump,0,SWT.LEFT);
		fd_btnShowPair.top = new FormAttachment(btnSetTrump,0,SWT.TOP);
		fd_btnShowPair.bottom = new FormAttachment(btnSetTrump,0,SWT.BOTTOM);
		btnShowPair.setLayoutData(fd_btnShowPair);
		
		group_info = new Group(shlPlay, SWT.NONE);
		group_info.setLayout(new FillLayout(SWT.HORIZONTAL));
		FormData fd_group_info = new FormData();
		fd_group_info.bottom = new FormAttachment(group_cards, 0, SWT.BOTTOM);
		fd_group_info.top = new FormAttachment(group_Play, 10);
		fd_group_info.right = new FormAttachment(group_Connection, 0, SWT.RIGHT);
		fd_group_info.left = new FormAttachment(group_Connection, 0, SWT.LEFT);
		group_info.setLayoutData(fd_group_info);
		
		TabFolder tabFolder = new TabFolder(group_info, SWT.NONE);
		
		tbtmChat = new TabItem(tabFolder, 0);
		tbtmChat.setText("Chat");
		
		group_chat = new Group(tabFolder, SWT.NONE);
		tbtmChat.setControl(group_chat);
		group_chat.setLayout(new FormLayout());
		
		tbChatOutput = new Text(group_chat, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		FormData fd_tbChatOutput = new FormData();
		fd_tbChatOutput.bottom = new FormAttachment(100, -25);
		fd_tbChatOutput.right = new FormAttachment(100);
		fd_tbChatOutput.top = new FormAttachment(0);
		fd_tbChatOutput.left = new FormAttachment(0);
		tbChatOutput.setLayoutData(fd_tbChatOutput);
		
		tbChatInput = new Text(group_chat, SWT.BORDER);
		tbChatInput.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				SendChatClick();
			}
		});
		FormData fd_tbChatInput = new FormData();
		fd_tbChatInput.left = new FormAttachment(tbChatOutput, 0, SWT.LEFT);
		fd_tbChatInput.bottom = new FormAttachment(100);
		fd_tbChatInput.top = new FormAttachment(tbChatOutput);
		fd_tbChatInput.right = new FormAttachment(70);
		tbChatInput.setLayoutData(fd_tbChatInput);
		
		btnChatSend = new Button(group_chat, SWT.NONE);
		btnChatSend.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SendChatClick();
			}
		});
		FormData fd_btnChatSend = new FormData();
		fd_btnChatSend.left = new FormAttachment(tbChatInput,0,SWT.RIGHT);
		fd_btnChatSend.bottom = new FormAttachment(100);
		fd_btnChatSend.top = new FormAttachment(tbChatOutput);
		fd_btnChatSend.right = new FormAttachment(100);
		btnChatSend.setLayoutData(fd_btnChatSend);
		btnChatSend.setText("Send");
		
		TabItem tbtmScore = new TabItem(tabFolder, SWT.NONE);
		tbtmScore.setText("Score");
		
		group_score = new Group(tabFolder, SWT.NONE);
		tbtmScore.setControl(group_score);
		group_score.setLayout(new FormLayout());
		
		lblTeam = new Label(group_score, SWT.BORDER);
		FormData fd_lblTeam = new FormData();
		fd_lblTeam.bottom = new FormAttachment(20);
		fd_lblTeam.right = new FormAttachment(60);
		fd_lblTeam.top = new FormAttachment(0);
		fd_lblTeam.left = new FormAttachment(0);
		lblTeam.setLayoutData(fd_lblTeam);
		lblTeam.setText("Team");
		
		lblSet = new Label(group_score, SWT.BORDER);
		FormData fd_lblSet = new FormData();
		fd_lblSet.bottom = new FormAttachment(20);
		fd_lblSet.right = new FormAttachment(80);
		fd_lblSet.top = new FormAttachment(0);
		fd_lblSet.left = new FormAttachment(60);
		lblSet.setLayoutData(fd_lblSet);
		lblSet.setText("Set");
		
		lblPoint = new Label(group_score, SWT.BORDER);
		FormData fd_lblPoint = new FormData();
		fd_lblPoint.bottom = new FormAttachment(20);
		fd_lblPoint.right = new FormAttachment(100);
		fd_lblPoint.top = new FormAttachment(0);
		fd_lblPoint.left = new FormAttachment(80);
		lblPoint.setLayoutData(fd_lblPoint);
		lblPoint.setText("Pt");
		
		lblEvenTeam = new Label(group_score, SWT.BORDER | SWT.WRAP);
		FormData fd_lblEvenTeam = new FormData();
		fd_lblEvenTeam.bottom = new FormAttachment(60);
		fd_lblEvenTeam.right = new FormAttachment(60);
		fd_lblEvenTeam.top = new FormAttachment(20);
		fd_lblEvenTeam.left = new FormAttachment(0);
		lblEvenTeam.setLayoutData(fd_lblEvenTeam);
		
		lblEvenTeamSet = new Label(group_score, SWT.BORDER);
		FormData fd_lblEvenTeamSet = new FormData();
		fd_lblEvenTeamSet.bottom = new FormAttachment(60);
		fd_lblEvenTeamSet.right = new FormAttachment(80);
		fd_lblEvenTeamSet.top = new FormAttachment(20);
		fd_lblEvenTeamSet.left = new FormAttachment(60);
		lblEvenTeamSet.setLayoutData(fd_lblEvenTeamSet);
		
		lblEvenTeamPoint = new Label(group_score, SWT.BORDER);
		FormData fd_lblEvenTeamPoint = new FormData();
		fd_lblEvenTeamPoint.bottom = new FormAttachment(60);
		fd_lblEvenTeamPoint.right = new FormAttachment(100);
		fd_lblEvenTeamPoint.top = new FormAttachment(20);
		fd_lblEvenTeamPoint.left = new FormAttachment(80);
		lblEvenTeamPoint.setLayoutData(fd_lblEvenTeamPoint);
		lblEvenTeamPoint.setText("");
		
		lblOddTeam = new Label(group_score, SWT.BORDER | SWT.WRAP);
		FormData fd_lblOddTeam = new FormData();
		fd_lblOddTeam.bottom = new FormAttachment(100);
		fd_lblOddTeam.right = new FormAttachment(60);
		fd_lblOddTeam.top = new FormAttachment(60);
		fd_lblOddTeam.left = new FormAttachment(0);
		lblOddTeam.setLayoutData(fd_lblOddTeam);
		
		lblOddTeamSet = new Label(group_score, SWT.BORDER);
		FormData fd_lblOddTeamSet = new FormData();
		fd_lblOddTeamSet.bottom = new FormAttachment(100);
		fd_lblOddTeamSet.right = new FormAttachment(80);
		fd_lblOddTeamSet.top = new FormAttachment(60);
		fd_lblOddTeamSet.left = new FormAttachment(60);
		lblOddTeamSet.setLayoutData(fd_lblOddTeamSet);
		
		lblOddTeamPoint = new Label(group_score, SWT.BORDER);
		FormData fd_lblOddTeamPoint = new FormData();
		fd_lblOddTeamPoint.bottom = new FormAttachment(100);
		fd_lblOddTeamPoint.right = new FormAttachment(100);
		fd_lblOddTeamPoint.top = new FormAttachment(60);
		fd_lblOddTeamPoint.left = new FormAttachment(80);
		lblOddTeamPoint.setLayoutData(fd_lblOddTeamPoint);
		
		TabItem tbtmLastHand = new TabItem(tabFolder, SWT.NONE);
		tbtmLastHand.setText("Last");
		
		group_lasthand = new Group(tabFolder, SWT.NONE);
		tbtmLastHand.setControl(group_lasthand);
		group_lasthand.setLayout(new FormLayout());
		
		lbl_LastHandSouth = new Label(group_lasthand, SWT.NONE);
		lbl_LastHandSouth.setAlignment(SWT.CENTER);
		lbl_LastHandSouth.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		FormData fd_lbl_LastHandSouth = new FormData();
		fd_lbl_LastHandSouth.bottom = new FormAttachment(100, -5);
		fd_lbl_LastHandSouth.right = new FormAttachment(60);
		fd_lbl_LastHandSouth.top = new FormAttachment(60, -5);
		fd_lbl_LastHandSouth.left = new FormAttachment(40);
		lbl_LastHandSouth.setLayoutData(fd_lbl_LastHandSouth);
		
		lbl_LastHandNorth = new Label(group_lasthand, SWT.NONE);
		lbl_LastHandNorth.setAlignment(SWT.CENTER);
		lbl_LastHandNorth.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		FormData fd_lbl_LastHandNorth = new FormData();
		fd_lbl_LastHandNorth.bottom = new FormAttachment(40, 5);
		fd_lbl_LastHandNorth.top = new FormAttachment(0, 5);
		fd_lbl_LastHandNorth.right = new FormAttachment(60);
		fd_lbl_LastHandNorth.left = new FormAttachment(40);
		lbl_LastHandNorth.setLayoutData(fd_lbl_LastHandNorth);
		
		lbl_LastHandEast = new Label(group_lasthand, SWT.NONE);
		lbl_LastHandEast.setAlignment(SWT.CENTER);
		lbl_LastHandEast.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		FormData fd_lbl_LastHandEast = new FormData();
		fd_lbl_LastHandEast.right = new FormAttachment(100, -5);
		fd_lbl_LastHandEast.left = new FormAttachment(80, -5);
		fd_lbl_LastHandEast.bottom = new FormAttachment(70);
		fd_lbl_LastHandEast.top = new FormAttachment(30);
		lbl_LastHandEast.setLayoutData(fd_lbl_LastHandEast);
		
		lbl_LastHandWest = new Label(group_lasthand, SWT.NONE);
		lbl_LastHandWest.setAlignment(SWT.CENTER);
		lbl_LastHandWest.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		FormData fd_lbl_LastHandWest = new FormData();
		fd_lbl_LastHandWest.right = new FormAttachment(20, 5);
		fd_lbl_LastHandWest.left = new FormAttachment(0, 5);
		fd_lbl_LastHandWest.bottom = new FormAttachment(70);
		fd_lbl_LastHandWest.top = new FormAttachment(30);
		lbl_LastHandWest.setLayoutData(fd_lbl_LastHandWest);
		
		group_BidResult = new Group(shlPlay, SWT.NONE);
		GridLayout gl_group_BidResult = new GridLayout(5, true);
		group_BidResult.setLayout(gl_group_BidResult);
		FormData fd_group_BidResult = new FormData();
		fd_group_BidResult.bottom = new FormAttachment(this.group_cards,-5,SWT.TOP);
		fd_group_BidResult.top = new FormAttachment(group_Play,5, SWT.BOTTOM);
		fd_group_BidResult.left = new FormAttachment(group_Play, 0, SWT.LEFT);
		fd_group_BidResult.right = new FormAttachment(group_Play, 0, SWT.RIGHT);
		
		group_SouthInfo = new Group(group_Play, SWT.NONE);
		group_SouthInfo.setEnabled(false);
		group_SouthInfo.setLayout(new FillLayout(SWT.HORIZONTAL));
		FormData fd_group_SouthInfo = new FormData();
		fd_group_SouthInfo.right = new FormAttachment(group_South, 40, SWT.RIGHT);
		fd_group_SouthInfo.top = new FormAttachment(group_South, -50);
		fd_group_SouthInfo.left = new FormAttachment(group_South, 5);
		fd_group_SouthInfo.bottom = new FormAttachment(group_South, 0, SWT.BOTTOM);
		group_SouthInfo.setLayoutData(fd_group_SouthInfo);
		
		btnSouthInfo = new Button(group_SouthInfo, SWT.NONE);
		
		group_EastInfo = new Group(group_Play, SWT.NONE);
		group_EastInfo.setEnabled(false);
		group_EastInfo.setLayout(new FillLayout(SWT.HORIZONTAL));
		FormData fd_group_EastInfo = new FormData();
		fd_group_EastInfo.left = new FormAttachment(group_East, -35);
		fd_group_EastInfo.top = new FormAttachment(group_East, -55, SWT.TOP);
		fd_group_EastInfo.bottom = new FormAttachment(group_East, -5);
		fd_group_EastInfo.right = new FormAttachment(group_East, 0, SWT.RIGHT);
		group_EastInfo.setLayoutData(fd_group_EastInfo);
		
		btnEastInfo = new Button(group_EastInfo, SWT.NONE);
		
		group_NorthInfo = new Group(group_Play, SWT.NONE);
		group_NorthInfo.setEnabled(false);
		group_NorthInfo.setLayout(new FillLayout(SWT.HORIZONTAL));
		FormData fd_group_NorthInfo = new FormData();
		fd_group_NorthInfo.bottom = new FormAttachment(group_North, 50);
		fd_group_NorthInfo.top = new FormAttachment(group_North, 0, SWT.TOP);
		fd_group_NorthInfo.left = new FormAttachment(group_North, -40, SWT.LEFT);
		fd_group_NorthInfo.right = new FormAttachment(group_North, -5);
		group_NorthInfo.setLayoutData(fd_group_NorthInfo);
		
		btnNorthInfo = new Button(group_NorthInfo, SWT.NONE);
		
		group_WestInfo = new Group(group_Play, SWT.NONE);
		group_WestInfo.setEnabled(false);
		group_WestInfo.setLayout(new FillLayout(SWT.HORIZONTAL));
		FormData fd_group_WestInfo = new FormData();
		fd_group_WestInfo.bottom = new FormAttachment(group_West, 55, SWT.BOTTOM);
		fd_group_WestInfo.top = new FormAttachment(group_West, 5);
		fd_group_WestInfo.right = new FormAttachment(group_West, 35);
		fd_group_WestInfo.left = new FormAttachment(group_West, 0, SWT.LEFT);
		group_WestInfo.setLayoutData(fd_group_WestInfo);
		
		btnWestInfo = new Button(group_WestInfo, SWT.NONE);
		group_BidResult.setLayoutData(fd_group_BidResult);
		tb_bidWinner = new Text(group_BidResult, SWT.MULTI | SWT.WRAP | SWT.READ_ONLY);
		tb_bidWinner.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		tb_bidWinner.setEditable(false);
		GridData gd_tb_bidWinner = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 2);
		gd_tb_bidWinner.widthHint = 50;
		tb_bidWinner.setLayoutData(gd_tb_bidWinner);
		lblDouble = new Label(group_BidResult, SWT.NONE);
		lblDouble.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		GridData gd_lblDouble = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblDouble.widthHint = 50;
		lblDouble.setLayoutData(gd_lblDouble);
		lblDouble.setAlignment(SWT.CENTER);
		lblDouble.setText("Double");
		lblReDouble = new Label(group_BidResult, SWT.NONE);
		lblReDouble.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		GridData gd_lblReDouble = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblReDouble.widthHint = 50;
		lblReDouble.setLayoutData(gd_lblReDouble);
		lblReDouble.setAlignment(SWT.CENTER);
		lblReDouble.setText("ReDouble");
		lblPairShown = new Label(group_BidResult, SWT.NONE);
		lblPairShown.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		GridData gd_lblPairShown = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblPairShown.widthHint = 50;
		lblPairShown.setLayoutData(gd_lblPairShown);
		lblPairShown.setAlignment(SWT.CENTER);
		lblPairShown.setText("Pair");
		label_SingleHand = new Label(group_BidResult, SWT.NONE);
		label_SingleHand.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
		GridData gd_label_SingleHand = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_label_SingleHand.widthHint = 50;
		label_SingleHand.setLayoutData(gd_label_SingleHand);
		label_SingleHand.setAlignment(SWT.CENTER);
		label_SingleHand.setText("Single Hand");

		label_DoubleName = new Label(group_BidResult, SWT.NONE);
		label_DoubleName.setAlignment(SWT.CENTER);
		label_DoubleName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		label_ReDoubleName = new Label(group_BidResult, SWT.NONE);
		label_ReDoubleName.setAlignment(SWT.CENTER);
		label_ReDoubleName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		label_PairName = new Label(group_BidResult, SWT.NONE);
		label_PairName.setAlignment(SWT.CENTER);
		label_PairName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblSingleHandName = new Label(group_BidResult, SWT.NONE);
		lblSingleHandName.setAlignment(SWT.CENTER);
		lblSingleHandName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		
		group_DoubleRe = new Group(shlPlay, SWT.NONE);
		FormData fd_group_DoubleRe = new FormData();
		fd_group_DoubleRe.bottom = new FormAttachment(group_Bid, 0, SWT.BOTTOM);
		fd_group_DoubleRe.top = new FormAttachment(group_Bid, 0, SWT.TOP);
		fd_group_DoubleRe.right = new FormAttachment(group_Bid, 0, SWT.RIGHT);
		fd_group_DoubleRe.left = new FormAttachment(group_Bid, 0, SWT.LEFT);
		group_DoubleRe.setLayoutData(fd_group_DoubleRe);
		group_DoubleRe.setLayout(new FormLayout());
		
		buttonReady23 = new Button(group_DoubleRe, SWT.NONE);
		buttonReady23.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Ready23ButtonClick();
			}
		});
		FormData fd_buttonReady23 = new FormData();
		fd_buttonReady23.right = new FormAttachment(0, 80);
		fd_buttonReady23.top = new FormAttachment(0, 8);
		fd_buttonReady23.left = new FormAttachment(0, 8);
		buttonReady23.setLayoutData(fd_buttonReady23);
		buttonReady23.setText("Ready");
		
		buttonDouble = new Button(group_DoubleRe, SWT.NONE);
		buttonDouble.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DoubleButtonClick();
			}
		});
		FormData fd_buttonDouble = new FormData();
		fd_buttonDouble.right = new FormAttachment(0, 80);
		fd_buttonDouble.top = new FormAttachment(0, 47);
		fd_buttonDouble.left = new FormAttachment(0, 8);
		buttonDouble.setLayoutData(fd_buttonDouble);
		buttonDouble.setText("2x");
		
		buttonRe = new Button(group_DoubleRe, SWT.NONE);
		buttonRe.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ReDoubleButtonClick();
			}
		});
		FormData fd_buttonRe = new FormData();
		fd_buttonRe.right = new FormAttachment(0, 163);
		fd_buttonRe.top = new FormAttachment(0, 47);
		fd_buttonRe.left = new FormAttachment(0, 91);
		buttonRe.setLayoutData(fd_buttonRe);
		buttonRe.setText("4x");
		
		buttonSingle = new Button(group_DoubleRe, SWT.NONE);
		buttonSingle.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SingleHandButtonClick();
			}
		});
		buttonSingle.setText("Single");
		FormData fd_buttonSingle = new FormData();
		fd_buttonSingle.left = new FormAttachment(buttonRe,0, SWT.LEFT);
		fd_buttonSingle.right = new FormAttachment(buttonRe,0, SWT.RIGHT);
		fd_buttonSingle.top = new FormAttachment(buttonReady23,0, SWT.TOP);
		fd_buttonSingle.bottom = new FormAttachment(buttonReady23,0, SWT.BOTTOM);
		buttonSingle.setLayoutData(fd_buttonSingle);



		CreateContentDynamic();
	}
	
	private void CreateContentDynamic(){
		FillLayout fillLayout = new FillLayout();
		fillLayout.type = SWT.HORIZONTAL;
		this.group_cards.setLayout(fillLayout);
		for(int i = 0 ; i<8 ; i++){
			arr_btn_Cards[i] = new Button(group_cards,SWT.NONE);
		}

		this.group_Bid.setVisible(true);
		this.group_DoubleRe.setVisible(false);
		btnShowPair.setVisible(false);
		this.btnSetTrump.setVisible(true);
		return;
	}
	private void ContentCreationDone(){
		arr_btngroup_Player[0] = btngroup_South;
		arr_btngroup_Player[1] = btngroup_East;
		arr_btngroup_Player[2] = btngroup_North;
		arr_btngroup_Player[3] = btngroup_West;
		
		arr_tbgroup_Player[0] = tbgroup_South2;
		arr_tbgroup_Player[1] = tbgroup_East2;
		arr_tbgroup_Player[2] = tbgroup_North2;
		arr_tbgroup_Player[3] = tbgroup_West2;
		
		arr_group_Player[0] = group_South;
		arr_group_Player[1] = group_East;
		arr_group_Player[2] = group_North;
		arr_group_Player[3] = group_West;
		
		arr_lblLastHands[0] = lbl_LastHandSouth;
		arr_lblLastHands[1] = lbl_LastHandEast;
		arr_lblLastHands[2] = lbl_LastHandNorth;
		arr_lblLastHands[3] = lbl_LastHandWest;
		
		arr_btnPlayerInfo[0] = btnSouthInfo;
		arr_btnPlayerInfo[1] = btnEastInfo;
		arr_btnPlayerInfo[2] = btnNorthInfo;
		arr_btnPlayerInfo[3] = btnWestInfo;
		
		btnConnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ConnectButtonClick();
			}
		});
		
		for(int i=0; i<4; i++)
			arr_btngroup_Player[i].addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				for(int i=0; i<4; i++){
					if(e.getSource()==arr_btngroup_Player[i])
						SeatClick(i);
				}
			}
		});
		
		for(int i=0; i<8; i++)
			arr_btn_Cards[i].addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				for(int i=0; i<8; i++){
					if(e.getSource()==arr_btn_Cards[i])
						CardClick(i);
				}
			}
		});
		
		btnBid.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				BidButtonClick(Integer.parseInt(comboBid.getItem(comboBid.getSelectionIndex())));
			}
		});
		
		btnPass.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PassButtonClick();
			}
		});
		
		shlPlay.addListener (SWT.Close, new Listener(){
			public void handleEvent (Event event) {
				CloseApp();
				}
		});

		//Display.getCurrent().update();
		Reload();
	}
	
	private void ShowPairClick(){
		client.SendShowPair();
		ShowBusy();
		return;
	}
	
	private void ShowTrumpButtonClick(){
		client.SendShowTrump();
		ShowBusy();
		return;
	}
	
	private void CardClick(int i){
		client.SendCardPlay(o_uidata.arr_cards[i]);
		ShowBusy();
		return;
	}
	private void SingleHandButtonClick(){
		client.SendReady3(1);
		ShowBusy();
		return;
	}
	
	private void ReDoubleButtonClick(){
		client.SendReady2(2);
		ShowBusy();
		return;
	}
	
	private void DoubleButtonClick(){
		client.SendReady2(1);
		ShowBusy();
		return;
	}
	private void Ready23ButtonClick(){
		if(o_uidata.b_isReady2Active)
			client.SendReady2(0);
		if(o_uidata.b_isReady3Active)
			client.SendReady3(0);
		ShowBusy();
		return;
	}
	
	private void SendChatClick(){
		if(!tbChatInput.getText().isEmpty()){
			client.SendChatMsg(tbChatInput.getText());
			tbChatInput.setText("");
		}
		return;
	}
	private void PassButtonClick(){
		client.SendBid(0);
		ShowBusy();
		return;
	}
	private void SetTrumpButtonClick(){
		if(o_uidata.i_TrumpClickNo==-1)
			return;
		client.SetTrump(o_uidata.i_TrumpClickNo);
		ShowBusy();
		return;
	}
	private void BidButtonClick(int iBid){
		client.SendBid(iBid);
		ShowBusy();
		return;
	}
	private void CloseApp(){
		if(client!=null)
			client.abort();
		if(o_uidata!=null)
			o_uidata.abort();
		return;
	}
	private void ConnectButtonClick(){
		client = new C29Client(this.tbName.getText(),this.tbServer.getText(),Integer.parseInt(this.tbPort.getText()));
		this.o_uidata.o_clientdata = (C29ClientData) client.o_clientdata;
		if(o_uidata.getState()==Thread.State.NEW)
			o_uidata.start();
		else{
			o_uidata = new UIDataThread(this);
			this.o_uidata.o_clientdata = (C29ClientData) client.o_clientdata;
			o_uidata.start();
		}
		client.start();
		ShowBusy();
		return;
	}
	private void btnReadyClick(){
		client.SendReady();
		ShowBusy();
	}
	
	private void ShowBusy(){
		BusyIndicator.showWhile(display, new Runnable(){
			public void run(){
				try {
					client.o_clientdata.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});		
	}
	private void SeatClick(int iSeatno){
		if(o_uidata.b_IsReady)
			return;
		if(client!=null&&client.o_clientdata.GetClientId()!=-1){
			client.SendSeatReq(iSeatno);
			ShowBusy();
		}
	}
	private void TrumpButtonClick(){
		o_uidata.i_TrumpClickNo = (o_uidata.i_TrumpClickNo + 1)%6;
		Reload();
	}
	private void Reload(){
		for(int i=0; i <4; i++ ){
			if(!arr_btngroup_Player[i].isDisposed())
				arr_btngroup_Player[i].setText(o_uidata.sPlayerName[i]);
			if(!arr_tbgroup_Player[i].isDisposed()){
				arr_tbgroup_Player[i].setText(o_uidata.sPlayerMsg[i]);
				if(o_uidata.e_gamestate29 == GameState29.PLAY_FIN){
					arr_tbgroup_Player[i].setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 8, SWT.NORMAL));
				}
				else
					arr_tbgroup_Player[i].setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.NORMAL));
				if(arr_tbgroup_Player[i].getImage()!=null)
					arr_tbgroup_Player[i].getImage().dispose();
				if(o_uidata.arr_tableCards[i]!=null){
					Image image = new Image(display,this.getClass().getResourceAsStream(GetImageFileName(o_uidata.arr_tableCards[i])));
					arr_tbgroup_Player[i].setImage(ResizeImage(arr_tbgroup_Player[i],image));
				}
			}
			if(!arr_btngroup_Player[i].isDisposed())
				arr_btngroup_Player[i].setEnabled(o_uidata.b_IsConnected & !o_uidata.b_IsReady & o_uidata.arr_bIsSeatFree[i] & o_uidata.b_IsConnected);
		}
		if(!tbServer.isDisposed())
			tbServer.setEnabled(!o_uidata.b_IsConnected);
		if(!tbName.isDisposed())
			tbName.setEnabled(!o_uidata.b_IsConnected);
		if(!tbPort.isDisposed())
			tbPort.setEnabled(!o_uidata.b_IsConnected);
		if(!btnConnect.isDisposed()){
			btnConnect.setEnabled(!o_uidata.b_IsConnected);
			if(!o_uidata.b_IsConnected)
				btnConnect.setText("Connect");
			else
				btnConnect.setText("Connected");
		}
		if(!btnReady.isDisposed())
			btnReady.setEnabled(o_uidata.b_IsSeated & !o_uidata.b_IsReady && o_uidata.b_IsConnected);
		if(!btnPass.isDisposed())
			btnPass.setEnabled((o_uidata.e_gamestate29 == GameState29.BIDDING) && (o_uidata.e_nextjobtype == NextJobTypes.BID) && o_uidata.b_IsConnected);
		if(!btnBid.isDisposed())
			btnBid.setEnabled((o_uidata.e_gamestate29 == GameState29.BIDDING) && (o_uidata.e_nextjobtype == NextJobTypes.BID) && o_uidata.b_IsConnected);
		if(!comboBid.isDisposed()){
			boolean enabled =(o_uidata.e_gamestate29 == GameState29.BIDDING) && (o_uidata.e_nextjobtype == NextJobTypes.BID) && o_uidata.b_IsConnected;
			comboBid.setEnabled(enabled);
			comboBid.removeAll();
			if(enabled){
				for(int i=o_uidata.i_minbid; i<30; i++){
					comboBid.add(Integer.toString(i));
				}
				comboBid.select(0);
			}
		}
		
		for(int i = 0; i<8; i++){
			if(!arr_btn_Cards[i].isDisposed()){
				if(arr_btn_Cards[i].getImage()!=null)
					arr_btn_Cards[i].getImage().dispose();
				arr_btn_Cards[i].setImage(null);
				arr_btn_Cards[i].setEnabled(false);
				if(o_uidata.arr_cards[i]!=null){
					Image image = new Image(display,this.getClass().getResourceAsStream(GetImageFileName(o_uidata.arr_cards[i])));
					arr_btn_Cards[i].setImage(ResizeImage(arr_btn_Cards[i],image));
					arr_btn_Cards[i].setEnabled(!o_uidata.arr_cards[i].IsSeventh());
						
				}									
			}
		}
		
		if(!this.group_cards.isDisposed()){
			group_cards.setEnabled(o_uidata.b_isCardsActive);
			if(o_uidata.b_isCardsActive)
				group_cards.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
			else
				group_cards.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		}
		if(!this.tb_bidWinner.isDisposed())
			tb_bidWinner.setText(o_uidata.sBidWinnerInfo);
		if(!this.groupCardColor.isDisposed()&&!this.btnCardColor.isDisposed()&&!btnTrump.isDisposed()){
			if(btnCardColor.getImage()!=null)
				btnCardColor.getImage().dispose();
			btnCardColor.setImage(null);
			btnCardColor.setText(new String(""));
			btnSetTrump.setEnabled(false);
			groupCardColor.setEnabled(false);
			this.btnTrump.setEnabled(o_uidata.b_isShowTrumpActive);
			if(o_uidata.e_gamestate29.ordinal() > GameState29.SETTING_TRUMP.ordinal()){//&& < Trump shown
				if(o_uidata.o_trump==null){
					if(!o_uidata.b_IsSeventh){
						Image image = new Image(display,this.getClass().getResourceAsStream(o_uidata.sCardNameBack));
						//btnCardColor.getParent().getParent().getParent().layout();
						//btnCardColor.getParent().getParent().layout();
						//btnCardColor.getParent().layout();
						btnCardColor.setImage(ResizeImage(btnCardColor,image));
					}
					else{
						btnCardColor.setText(new String("7"));
						btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
						btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
					}
				}
				else{
					if(!o_uidata.b_IsSeventh){
						switch(o_uidata.o_trump.GetSuit()){
						case 0:
							btnCardColor.setText(new String("\u2665"));
							btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
							break;
						case 1:
							btnCardColor.setText(new String("\u2663"));
							btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
							break;
						case 2:
							btnCardColor.setText(new String("\u2666"));
							btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
							break;
						case 3:
							btnCardColor.setText(new String("\u2660"));
							btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
							break;
						case 4:
							btnCardColor.setText(new String("\u2603"));
							btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
							break;
						}
					}
					else{
						btnCardColor.setText(new String(o_uidata.o_trump.toString()));
						btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 12, SWT.BOLD));
						if(o_uidata.o_trump.GetSuit() %2 ==0)
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
						else
							btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
					}
				}
			}
			if(o_uidata.e_gamestate29==GameState29.SETTING_TRUMP && o_uidata.e_nextjobtype == NextJobTypes.SET_TRUMP){
				if(!btnSetTrump.isDisposed())
					btnSetTrump.setEnabled(true);
				groupCardColor.setEnabled(true);

				switch(o_uidata.i_TrumpClickNo){
				case -1:
					Image image = new Image(display,this.getClass().getResourceAsStream(o_uidata.sCardNameBack));
					//btnCardColor.setImage(image);
					//btnCardColor.getParent().getParent().getParent().layout();
					//btnCardColor.getParent().getParent().layout();
					//btnCardColor.getParent().layout();
					btnCardColor.setImage(ResizeImage(btnCardColor,image));
					break;
				case 0:
					btnCardColor.setText(new String("\u2665"));
					btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
					btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
					break;
				case 1:
					btnCardColor.setText(new String("\u2663"));
					btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
					btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
					break;
				case 2:
					btnCardColor.setText(new String("\u2666"));
					btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
					btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
					break;
				case 3:
					btnCardColor.setText(new String("\u2660"));
					btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
					btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
					break;
				case 4:
					btnCardColor.setText(new String("\u2603"));
					btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
					btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
					break;
				case 5:
					btnCardColor.setText(new String("7"));
					btnCardColor.setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
					btnCardColor.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
					break;
				}
			}
		}
		if(!btnChatSend.isDisposed())
			btnChatSend.setEnabled(o_uidata.b_IsConnected);
		if(!tbChatInput.isDisposed())
			tbChatInput.setEnabled(o_uidata.b_IsConnected);
		if(!tbChatOutput.isDisposed())
			tbChatOutput.append(o_uidata.s_ChatMsg);
		
		if(!group_Bid.isDisposed())
			group_Bid.setVisible(o_uidata.b_groupbidvisible);
		if(!group_DoubleRe.isDisposed())
			this.group_DoubleRe.setVisible(o_uidata.b_groupdoublevisible);
		
		if(!this.buttonReady23.isDisposed())
			buttonReady23.setEnabled(o_uidata.b_isReady2Active || o_uidata.b_isReady3Active);
		if(!this.buttonDouble.isDisposed())
			buttonDouble.setEnabled(o_uidata.b_isDoubleActive);
		if(!this.buttonRe.isDisposed())
			buttonRe.setEnabled(o_uidata.b_isReDoubleActive);
		if(!this.label_DoubleName.isDisposed())
			label_DoubleName.setText(o_uidata.sDoubleName);
		if(!this.label_ReDoubleName.isDisposed())
			label_ReDoubleName.setText(o_uidata.sRedoubleName);
		
		if(!this.buttonSingle.isDisposed())
			buttonSingle.setEnabled(o_uidata.b_isSingleActive);
		if(!this.lblSingleHandName.isDisposed())
			lblSingleHandName.setText(o_uidata.sSingleName);
		
		
		for(int i=0; i<4;i++){
			if(o_uidata.i_handwinner == i && !arr_tbgroup_Player[i].isDisposed())
				arr_tbgroup_Player[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
			if(o_uidata.i_handwinner != i && !arr_tbgroup_Player[i].isDisposed())
				arr_tbgroup_Player[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		}
		
		if(o_uidata.i_gamewinnerParity!=-1){
			for(int i=0; i<4;i++){
				if((i%2 == o_uidata.i_gamewinnerParity ) && !arr_tbgroup_Player[i].isDisposed())
					arr_tbgroup_Player[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
				else if(!arr_tbgroup_Player[i].isDisposed())
					arr_tbgroup_Player[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
			}
		}
		
		ReloadPairInfo();
		ReloadLastHands();
		ReloadPlayerInfo();
		ReloadPoints();
	}
	
	private void ReloadPairInfo(){
		if(!this.label_PairName.isDisposed())
			this.label_PairName.setText(o_uidata.sShowPairName);
		if(!this.btnSetTrump.isDisposed())
			btnSetTrump.setVisible(o_uidata.b_isSetTrumpVisible);
		if(!this.btnShowPair.isDisposed()){
			btnShowPair.setVisible(!o_uidata.b_isSetTrumpVisible);
			btnShowPair.setEnabled(o_uidata.b_isShowPairActive);
		}		
	}
	private void ReloadPoints(){
		if(!lblEvenTeam.isDisposed())
			lblEvenTeam.setText(o_uidata.sTeamEven);
		if(!lblOddTeam.isDisposed())
			lblOddTeam.setText(o_uidata.sTeamOdd);
		if(!lblEvenTeamPoint.isDisposed())
			lblEvenTeamPoint.setText(o_uidata.sEvenPt);
		if(!lblOddTeamPoint.isDisposed())
			lblOddTeamPoint.setText(o_uidata.sOddPt);
		if(!lblEvenTeamSet.isDisposed())
			lblEvenTeamSet.setText(o_uidata.sEvenSet);
		if(!lblOddTeamSet.isDisposed())
			lblOddTeamSet.setText(o_uidata.sOddSet);
	}	
	
	private void ReloadPlayerInfo(){
		for(int i=0; i<4; i++){
			if(this.arr_btnPlayerInfo[i].isDisposed())
				continue;
			arr_btnPlayerInfo[i].setText(o_uidata.sPlayerInfo[i]);
			arr_btnPlayerInfo[i].setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 20, SWT.BOLD));
		}
	}
	private void ReloadLastHands(){
		for(int i = 0; i<4 ; i++){
			if(arr_lblLastHands[i].isDisposed())
				continue;
			if(o_uidata.arr_lastCards[i] == null){
				arr_lblLastHands[i].setText("");
				arr_lblLastHands[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
				continue;
			}
			this.arr_lblLastHands[i].setText(o_uidata.arr_lastCards[i].toString());
			arr_lblLastHands[i].setFont(SWTResourceManager.getFont(display.getSystemFont().getFontData()[0].getName(), 11, SWT.BOLD));
			if(o_uidata.arr_lastCards[i].GetSuit() %2 ==0)
				arr_lblLastHands[i].setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
			else
				arr_lblLastHands[i].setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
			if(i!=o_uidata.i_lastwinner)
				arr_lblLastHands[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
			else
				arr_lblLastHands[i].setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_YELLOW));
		}
		
	}
	public void update(){
		if(!display.isDisposed())
			display.asyncExec(new Runnable() {
		        public void run() {
		        	Reload();
		        }
		      });
	}
	private Image ResizeImage(Control btn, Image img){
		Image ret;
		int width, height;
		//System.out.println("Image Width "+img.getImageData().width + "Image Height " + img.getImageData().height);
		if(((double)btn.getSize().x) / img.getImageData().width > ((double)btn.getSize().y) / img.getImageData().height){
			height = (int) (btn.getSize().y * 0.8);
			width = (int) (img.getImageData().width * (((double)btn.getSize().y) / img.getImageData().height) * 0.8);
		}
		else{
			width = (int) (btn.getSize().x * 0.8);
			height = (int) (img.getImageData().height * (((double)btn.getSize().x) / img.getImageData().width) * 0.8);
		}
		//System.out.println("Button Size "+ btn.getSize().x + ", " + btn.getSize().y);
		//System.out.println("New Image Width "+ width + " New Image Height " + height);
		ret = new Image(img.getDevice(),img.getImageData().scaledTo(width, height));
		img.dispose();
		return ret;
	}
	private String GetImageFileName(CCard card){
		String suit;
		String value;
		switch(card.GetSuit()){
		case 1:
			suit = "clubs-";break;
		case 2:
			suit = "diamonds-";break;
		case 0:
			suit = "hearts-"; break;
		case 3:
			suit = "spades-";break;
		default:
			suit ="Error";
		}
		switch(card.GetValue()){
		case 1:
			value = "a-";break;
		case 11:
			value = "j-";break;
		case 12:
			value = "q-";break;
		case 13:
			value = "k-";break;
		default:
			value = Integer.toString(card.GetValue()) + "-";
		}
		return "75/"+ suit + value + "75.png";
	}
}
