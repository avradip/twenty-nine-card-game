package gui;
import java.util.ListIterator;

import common.*;
import common.CMessage29.NextJobTypes;
import common.CMessage29.GameState29;
import client.*;


public class UIDataThread extends Thread{
	String sPlayerName[];
	String sPlayerMsg[];
	String sBidWinnerInfo;
	String sCardNameBack;
	boolean arr_bIsSeatFree[];
	C29ClientData o_clientdata;
	FormMain formmain;
	boolean b_IsConnected;
	boolean b_running;
	boolean b_IsReady;
	boolean b_IsSeated;
	GameState29 e_gamestate29;
	NextJobTypes e_nextjobtype;
	boolean b_IsSeventh;
	String s_ChatMsg;
	int i_minbid;
	CCard29 arr_cards[];
	int i_TrumpClickNo;
	boolean b_isDoubleActive;
	boolean b_isReDoubleActive;
	boolean b_isReady2Active;
	String sDoubleName;
	String sRedoubleName;
	boolean b_groupbidvisible;
	boolean b_groupdoublevisible;
	boolean b_isReady3Active;
	boolean b_isSingleActive;
	boolean b_isCardsActive;
	boolean b_isShowPairActive;
	boolean b_isShowTrumpActive;
	boolean b_isSetTrumpVisible;
	int 	i_handwinner;
	String sSingleName;
	CCard29 arr_tableCards[];
	CCard29 o_trump;
	CCard29 arr_lastCards[];
	int 	i_lastwinner;
	String sPlayerInfo[];
	int i_gamewinnerParity;
	String sShowPairName;
	
	String sTeamEven;
	String sTeamOdd;
	String sEvenSet;
	String sOddSet;
	String sEvenPt;
	String sOddPt;
	
	public UIDataThread(FormMain form){
		formmain = form;
		b_IsConnected = false;
		b_running = false;
		b_IsReady = false;
		b_IsSeated = false;
		sPlayerName = new String[4];
		sPlayerMsg = new String[4];
		arr_bIsSeatFree = new boolean[4];
		arr_tableCards = new CCard29[4];
		arr_lastCards = new CCard29[4];
		sPlayerInfo = new String[4];
		for(int i=0; i<4; i++){
			sPlayerName[i] = "Free";
			sPlayerMsg[i] = "";
			arr_bIsSeatFree[i] = true;
			sPlayerInfo[i] = "";
		}
		arr_cards = new CCard29[8];
		sCardNameBack = "75/back-red-75-1.png";
		
		e_gamestate29 = GameState29.UNDEFINED;
		s_ChatMsg = "";
		
		this.sDoubleName = "None";
		this.sRedoubleName = "None";
		sShowPairName = "None";
		this.b_isDoubleActive = false;
		this.b_isReady2Active = false;
		this.b_isReDoubleActive = false;
		this.sBidWinnerInfo = "";
		b_groupdoublevisible = false;
		b_groupbidvisible = true;
		b_isReady3Active = false;
		b_isSingleActive = false;
		b_isCardsActive = false;
		b_isShowPairActive = false;
		b_isShowTrumpActive =false;
		b_isSetTrumpVisible = true;
		
		sSingleName = "None";
		i_handwinner = -1;
		o_trump = null;
		i_gamewinnerParity = -1;
		
		sTeamEven = sTeamOdd = sEvenSet = sOddSet = sEvenPt = sOddPt = "";
	}	
	
	//Ideally this function should copy everything from clientdata...
	public void Reload(){
		if(o_clientdata.GetClientId()==-1){
			b_running=false;
			b_IsConnected = false;
		}
		else
			b_IsConnected = true;
		
		for(int i=0; i<4; i++){
			int id = ((C29ClientData)o_clientdata).GetIdOnSeat(i);
			if(id>=0 && id < 4){
				sPlayerName[i] = ((C29ClientData)o_clientdata).GetConnectedClientName(id);
				arr_bIsSeatFree[i] = false;
				if(((C29ClientData)o_clientdata).GetReadyNess(id))
					sPlayerMsg[i] = "Ready";
				else 
					sPlayerMsg[i] = "";
			}
			else{ 
				sPlayerName[i] = "Free";
				sPlayerMsg[i] = "";
				arr_bIsSeatFree[i] = true;
			}
			arr_tableCards[i] = null;
			arr_lastCards[i] = null;
		}
		this.sDoubleName = "None";
		this.sRedoubleName = "None";
		this.sSingleName = "None";
		sShowPairName = "None";
		this.b_isDoubleActive = false;
		this.b_isReady2Active = false;
		this.b_isReDoubleActive = false;
		this.sBidWinnerInfo = "";
		b_groupdoublevisible = false;
		b_groupbidvisible = true;
		b_isReady3Active = false;
		b_isSingleActive = false;
		b_isCardsActive = false;
		b_isShowPairActive = false;
		b_isShowTrumpActive =false;
		i_handwinner = -1;
		o_trump = null;
		i_gamewinnerParity = -1;
		sTeamEven = sTeamOdd = sEvenSet = sOddSet = sEvenPt = sOddPt = "";
		b_isSetTrumpVisible = true;
		
		if(o_clientdata.GetClientId()!=-1){
			s_ChatMsg = o_clientdata.GetChatMsg();
			b_IsReady = ((C4PlayerGameClientData)o_clientdata).GetReadyNess(o_clientdata.GetClientId());
			if(((C4PlayerGameClientData)o_clientdata).GetSeatForId(o_clientdata.GetClientId()) !=-1)
				b_IsSeated = true;
			else
				b_IsSeated = false;
			
			for(int i=0; i<8;i++)
				arr_cards[i] = null;
			
			
			e_gamestate29 = o_clientdata.GetGameState();
			if(e_gamestate29.ordinal() > GameState29.SETTING_TRUMP.ordinal()){
				b_groupdoublevisible = true;
				b_groupbidvisible = false;
			}
			if(e_gamestate29 == GameState29.PLAY_FIN){
				b_groupdoublevisible = false;
				b_groupbidvisible = true;				
			}
			b_IsSeventh = o_clientdata.IsSeventh();
			o_trump = o_clientdata.GetTrumpCard();
			if(e_gamestate29 == GameState29.BIDDING)
				i_TrumpClickNo = -1;
			for(int i=0; i<((C29ClientData)o_clientdata).GetMyCards().size(); i++)
				 arr_cards[i]=((C29ClientData)o_clientdata).GetMyCards().get(i);
			
			if(((C29ClientData)o_clientdata).GetNextJobs()!=null){
				e_nextjobtype = o_clientdata.GetNextJobs()[o_clientdata.GetClientId()];
				for(int i=0; i<4; i++){
					switch(((C29ClientData)o_clientdata).GetNextJobs()[i]){
					case WAIT:
						switch(e_gamestate29){
						case BIDDING:
						case SETTING_TRUMP:
							if(((C29ClientData)o_clientdata).GetCurrentBids()[i]==-1)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Waiting";
							else 
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] 
									= "Waiting, bidded " + Integer.toString(((C29ClientData)o_clientdata).GetCurrentBids()[i]);
							break;
						case WAITING_4_OK2:
							if(((C29ClientData)o_clientdata).GetCurrentDoubleRedouble(i) == -1)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Waiting";
							if(((C29ClientData)o_clientdata).GetCurrentDoubleRedouble(i) == 0)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Ready";
							if(((C29ClientData)o_clientdata).GetCurrentDoubleRedouble(i) == 1)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Double";
							if(((C29ClientData)o_clientdata).GetCurrentDoubleRedouble(i) == 2)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "ReDouble";
							break;
						case WAITING_4_OK3:
							if(((C29ClientData)o_clientdata).GetCurrentReady3(i) == -1)
								//This means somebody else called single hand before this client decided on anything
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Ready"; 
							if(((C29ClientData)o_clientdata).GetCurrentReady3(i) == 0)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Ready";
							if(((C29ClientData)o_clientdata).GetCurrentReady3(i) == 1)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Single Hand";
							break;
						case PLAYING:
						case PLAY_PAUSED:
							arr_tableCards[o_clientdata.GetSeatForId(i)] = o_clientdata.GetPlayedCard(i);
							if(arr_tableCards[o_clientdata.GetSeatForId(i)]==null)
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Waiting";
							break;
						case PLAY_FIN:
							sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] 
									+= "Total " + Integer.toString(o_clientdata.GetGameCompletionInfo().arr_points[i]) + "\n";
							ListIterator<CCard29> it = o_clientdata.GetGameCompletionInfo().list_listwincards.get(i).listIterator();
							while(it.hasNext())
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] += (it.next().toString());
							if(i == o_clientdata.GetLastHandWinner())
								sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] += "+1";
							
							break;
						default:
							sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Error";
							
						}
						break;
						
					case BID:
						sPlayerMsg[o_clientdata.GetSeatForId(i)] = "Bidding";
						break;
					case SET_TRUMP:
						sPlayerMsg[o_clientdata.GetSeatForId(i)] = "Setting Color";
						break;
					case GET_READY2:
						if((o_clientdata.GetSeatForId(i) - o_clientdata.GetSeatForId(o_clientdata.GetBidWinnerId()))%2==0)
							sPlayerMsg[o_clientdata.GetSeatForId(i)] = "Deciding on Redouble";
						else
							sPlayerMsg[o_clientdata.GetSeatForId(i)] = "Deciding on Double";
						break;
					case GET_READY3:
						sPlayerMsg[o_clientdata.GetSeatForId(i)] = "Deciding on Single Hand";
						break;
					case PLAY:
						sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Playing";
						break;
					default:
							sPlayerMsg[((C4PlayerGameClientData)o_clientdata).GetSeatForId(i)] = "Error";
					}
				}
				if((e_nextjobtype == NextJobTypes.BID)&&(e_gamestate29 == GameState29.BIDDING))
					i_minbid = ((C29ClientData)o_clientdata).getI_nextminbid();

				if(e_gamestate29 == GameState29.WAITING_4_OK2){
					if(e_nextjobtype == NextJobTypes.GET_READY2)
						if((o_clientdata.GetSeatForId(o_clientdata.GetClientId()) - o_clientdata.GetSeatForId(o_clientdata.GetBidWinnerId()))%2==0){
							this.b_isReDoubleActive = true;
							this.b_isReady2Active = true;
						}
						else{
							this.b_isDoubleActive = true;
							this.b_isReady2Active = true;
						}
				}

				for(int i =0; i<4; i++)
					if(o_clientdata.GetCurrentDoubleRedouble(i) == 1)
						sDoubleName = o_clientdata.GetConnectedClientName(i);
					else if(o_clientdata.GetCurrentDoubleRedouble(i) == 2)
						sRedoubleName = o_clientdata.GetConnectedClientName(i);
				
				if(e_gamestate29 == GameState29.WAITING_4_OK3 && e_nextjobtype == NextJobTypes.GET_READY3){
					this.b_isSingleActive = true;
					this.b_isReady3Active = true;
				}
					
				if(e_gamestate29 == GameState29.PLAYING && e_nextjobtype == NextJobTypes.PLAY){
					b_isCardsActive = true;
					if(o_trump == null)
						b_isShowTrumpActive =true;
					if(o_clientdata.GetClientIdPair()==-1 && o_trump!=null) //Still this does not take care of the fact, ShowPair should be inactive even when trump is out in the same round
						b_isShowPairActive = true;
				}
				for(int i = 0; i<4; i++)
					if(o_clientdata.GetCurrentReady3(i) == 1)
						sSingleName = o_clientdata.GetConnectedClientName(i);
				
				if(e_gamestate29 == GameState29.PLAY_FIN)
					i_gamewinnerParity = o_clientdata.GetGameCompletionInfo().winner%2;
						
			}

			
			if(e_gamestate29.ordinal() > GameState29.BIDDING.ordinal())
				this.sBidWinnerInfo = o_clientdata.GetConnectedClientName(o_clientdata.GetBidWinnerId()) + " " +  o_clientdata.GetWinningBid();

			if(e_gamestate29 == GameState29.PLAY_PAUSED)
				this.i_handwinner = o_clientdata.GetSeatForId(o_clientdata.GetHandWinner());
			else
				i_handwinner =-1;
			
			for(int i=0;i<4; i++){
				if(o_clientdata.GetSeatForId(i)!=-1)
					arr_lastCards[o_clientdata.GetSeatForId(i)] = o_clientdata.GetLastTableCard(i);
			}
			this.i_lastwinner = o_clientdata.GetSeatForId(o_clientdata.GetLastHandWinner());
			
			
			for(int i =0; i<4; i++)
				if(o_clientdata.GetSeatForId(i)!=-1)
					sPlayerInfo[o_clientdata.GetSeatForId(i)] = Integer.toString(o_clientdata.GetHandWonCnt(i));
			
			for(int i =0; i<4; i++){
				if((i%2 == 0) && (o_clientdata.GetIdOnSeat(i) !=-1)){
					this.sTeamEven += o_clientdata.GetConnectedClientName(o_clientdata.GetIdOnSeat(i));
					this.sTeamEven += " ";
				}
				if((i%2 == 1) && (o_clientdata.GetIdOnSeat(i) !=-1)){
					this.sTeamOdd += o_clientdata.GetConnectedClientName(o_clientdata.GetIdOnSeat(i));
					this.sTeamOdd += " ";
				}
			}
			
			this.sEvenPt = Integer.toString(o_clientdata.GetGamePointInfo().EvenPoint);
			this.sEvenSet = Integer.toString(o_clientdata.GetGamePointInfo().EvenSet);
			this.sOddPt = Integer.toString(o_clientdata.GetGamePointInfo().OddPoint);
			this.sOddSet = Integer.toString(o_clientdata.GetGamePointInfo().OddSet);
			
			if(o_clientdata.GetClientIdPair() != -1 && e_gamestate29.ordinal() > GameState29.SETTING_TRUMP.ordinal()){
				sShowPairName = o_clientdata.GetConnectedClientName(o_clientdata.GetClientIdPair());
				if(o_clientdata.GetBidChange() > 0){
					this.sBidWinnerInfo += "+" + Integer.toString(o_clientdata.GetBidChange());
				}
				else
					this.sBidWinnerInfo += " - " + Integer.toString(- o_clientdata.GetBidChange());
			}
			
			if((e_gamestate29.ordinal() > GameState29.SETTING_TRUMP.ordinal()) && (e_gamestate29.ordinal() != GameState29.PLAY_FIN.ordinal()))
				this.b_isSetTrumpVisible = false;
			else
				b_isSetTrumpVisible = true;
		}
		formmain.update();
	}

	public void run(){
		b_running = true;
		while(b_running){
			try {
				o_clientdata.await();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Reload();
		}
		System.out.println("UIDataThread::Run::Abort Completed");
	}
	public void abort(){
		b_running = false;
		if(o_clientdata!=null)
			o_clientdata.signal();
		System.out.println("UIDataThread::abort()");
	}
}
