package client;

import common.*;

public class C29Client extends C4PlayerGameClient {
	
	public C29Client(String name, String serverip, int port){
		super(name,serverip,port,true);
		this.o_clientdata = new C29ClientData();
	}
	
	@Override
	protected void ProcessMessage(Object obj){
		try{
			CMessage29 msg = (CMessage29)obj;
			System.out.println("C29Client:ProcessMessage. MsgId " + msg.l_msgid);	
			
			switch(msg.e_datatype29){
				case FOUR_CARDS:
					((C29ClientData)o_clientdata).UpdateMyCards(msg);
					break;
				case FOUR_CARDS2:
					((C29ClientData)o_clientdata).UpdateMyCards2(msg);
					break;
				case BID_COMPLETION_INFO:
					((C29ClientData)o_clientdata).UpdateBidCompletionInfo(msg);
					break;
				case JOB_DONE_BID:
					((C29ClientData)o_clientdata).UpdateBid(msg);
					break;
				case JOB_DONE_SET_TRUMP:
					((C29ClientData)o_clientdata).UpdateTrumpSetCompletionInfo(msg);
					break;
				case JOB_DONE_GET_READY2:
					((C29ClientData)o_clientdata).UpdateReady2(msg);
					break;
				case JOB_DONE_GET_READY3:
					((C29ClientData)o_clientdata).UpdateReady3(msg);
					break;
				case START_PLAY:
					((C29ClientData)o_clientdata).UpdateStartPlay(msg);
					break;
				case JOB_DONE_PLAY_CARD:
					((C29ClientData)o_clientdata).UpdatePlayedCard(msg);
					break;
				case HAND_WINNER_INFO:
					((C29ClientData)o_clientdata).UpdateHandWinnerInfo(msg);
					break;					
				case JOB_DONE_BAD_PLAY:
					((C29ClientData)o_clientdata).UpdateBadPlayInfo(msg);
					break;				
				case JOB_DONE_PLAY_SHOWTRUMP:
					((C29ClientData)o_clientdata).UpdatePlayedShowTrump(msg);
					break;
				case GAME_COMPLETION_INFO:
					((C29ClientData)o_clientdata).UpdateGameCompletionInfo(msg);
					break;	
				case JOB_DONE_PLAY_PAIR:
					((C29ClientData)o_clientdata).UpdateShowPair(msg);
					break;	
				default:
					System.err.println("C29Client::ProcessMessage:: Unknown MsgType");
			}
		}
		catch(ClassCastException e){
			super.ProcessMessage(obj);
		}
	}
	
	public void SendBid(int i_Bid){
		CMessage29 msg = CMessage29.CeateMsg_Bid_Client(i_Bid, o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
		return;
	}
	
	public void SetTrump(int iSuit){
		CMessage29 msg = CMessage29.CreateMsg_SetTrump_Client(iSuit, o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
		return;
	}
	
	public void SendReady2(int iReadyNess){
		CMessage29 msg = CMessage29.CreateMsg_JobDoneGetReady2_Client(iReadyNess, o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
		return;		
	}
	
	public void SendReady3(int iReadyNess){
		CMessage29 msg = CMessage29.CreateMsg_JobDoneGetReady3_Client(iReadyNess, o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
		return;		
	}
	
	public void SendCardPlay(CCard29 card){
		CMessage29 msg = CMessage29.CreateMsg_JobDonePlayCard_Client( o_clientdata.GetClientId(), this.i_outmsg_cnt++, card);
		q_msg_out.add(msg);
		return;	
	}
	
	public void SendShowTrump(){
		CMessage29 msg = CMessage29.CreateMsg_JobDonePlayShowTrump_Client(o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
		return;	
	}
	
	public void SendShowPair(){
		CMessage29 msg = CMessage29.CreateMsg_JobDonePlayPair_Client(o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
		return;	
	}
}
