package client;


import java.util.*;

import common.*;
import common.CMessage29.GameState29;
import common.CMessage29.NextJobTypes;

public class C29ClientData extends C4PlayerGameClientData {
	private LinkedList<CCard29> list_mycards; public LinkedList<CCard29> GetMyCards(){return list_mycards;}
	private NextJobTypes[] arr_nextjob; public NextJobTypes[] GetNextJobs(){return arr_nextjob;}
	
	
	private int i_nextminbid;
	private int arr_iClientBids[];
	private CCard29 arr_currentTable[]; public CCard29 GetPlayedCard(int iClientid){return arr_currentTable[iClientid];}
	
	private int i_BidWinnerId; public int GetBidWinnerId(){ return i_BidWinnerId;}
	private int i_WinningBid;  public int GetWinningBid() { return i_WinningBid;}
	public 	int i_HandWinner; public int GetHandWinner() 	{return i_HandWinner;}
	
	private CCard29 arr_lastTable[]; public CCard29 GetLastTableCard(int iClientid){return arr_lastTable[iClientid];}
	private int i_lasthandWinner; public int GetLastHandWinner(){return i_lasthandWinner;}
	
	public int[] GetCurrentBids(){return arr_iClientBids;}
	public int getI_nextminbid() {
		return i_nextminbid;
	}
	
	private GameState29 e_GameState29; public GameState29 GetGameState(){return e_GameState29;}
	
	private boolean b_isSeventh; public boolean IsSeventh(){ return b_isSeventh;}
	
	private int arr_iClientDoubleRedouble[]; public int GetCurrentDoubleRedouble(int iPlayerId){return arr_iClientDoubleRedouble[iPlayerId];}
	private int arr_iClientReady3[]; public int GetCurrentReady3(int iPlayerId){return arr_iClientReady3[iPlayerId];}
	
	private CCard29 o_trump; public CCard29 GetTrumpCard(){return o_trump;}
	
	private int arr_iHandWonCnt[]; public int GetHandWonCnt(int iClientid){return arr_iHandWonCnt[iClientid];}
	
	private CGamePointInfo o_gamepoint; public CGamePointInfo GetGamePointInfo(){return o_gamepoint;}
	private CGameCompletionInfo o_gameCompletionInfo; public CGameCompletionInfo GetGameCompletionInfo(){return o_gameCompletionInfo;}
	
	private int i_PairShowClientId; public int GetClientIdPair(){return i_PairShowClientId;}
	private int i_bidChange;		public int GetBidChange(){return i_bidChange;}
	
	public C29ClientData(){
		super();
		list_mycards = new LinkedList<CCard29>();
		arr_iClientBids = new int[4];
		arr_iClientDoubleRedouble = new int[4];
		arr_iClientReady3 = new int[4];
		arr_currentTable = new CCard29[4];
		arr_lastTable = new CCard29[4];
		arr_iHandWonCnt = new int[4];
		e_GameState29 = GameState29.UNDEFINED;
		o_gamepoint = new CGamePointInfo();
	}
	
	private void ResetClientData(){
		list_mycards.clear();
		i_BidWinnerId = -1;
		i_WinningBid = 15;
		for(int i = 0; i<4; i++){
			arr_iClientBids[i] =-1;
			arr_iClientDoubleRedouble[i] = -1;
			arr_iClientReady3[i] = -1;
		}
		e_GameState29 = GameState29.BIDDING;
		i_nextminbid = -1;
		b_isSeventh = false;
		o_trump = null;
		
		for(int i=0 ; i<4 ;i++){
			arr_currentTable[i]=null;
			arr_lastTable[i] = null;
			arr_iHandWonCnt[i] = 0;
		}
			
		i_HandWinner = -1;
		i_lasthandWinner = -1;
		
		o_gameCompletionInfo = null;
	
		i_PairShowClientId = -1;
		i_bidChange = 0;		
	}
	
	public void UpdateMyCards(CMessage29 msg){
		ResetClientData();
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		for(int i=0; i<((CCard29[])msg.arr_data[1]).length ;i++)
			list_mycards.add(((CCard29[])msg.arr_data[1])[i]);
		i_nextminbid = ((Integer)msg.arr_data[2]).intValue();
		Collections.sort(list_mycards);
		this.signal();
	}
	
	public void UpdateMyCards2(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		for(int i=0; i<((CCard29[])msg.arr_data[1]).length ;i++)
			list_mycards.add(((CCard29[])msg.arr_data[1])[i]);
		Collections.sort(list_mycards);
		e_GameState29 = GameState29.WAITING_4_OK3;
		this.signal();
	}
	public void UpdateBid(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		if(((Integer)((Object[]) msg.arr_data[1])[1]).intValue()!=0)
			arr_iClientBids[((Integer)((Object[]) msg.arr_data[1])[0]).intValue()] = ((Integer)((Object[]) msg.arr_data[1])[1]).intValue();
		i_nextminbid = ((Integer)msg.arr_data[2]).intValue();
		this.signal();		
	}

	public void UpdateBidCompletionInfo(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		i_BidWinnerId = ((Integer)msg.arr_data[1]).intValue();
		i_WinningBid = ((Integer)msg.arr_data[2]).intValue();
		e_GameState29 = GameState29.SETTING_TRUMP;
		this.signal();
	}
	
	public void UpdateTrumpSetCompletionInfo(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		b_isSeventh = ((Boolean)msg.arr_data[2]).booleanValue();
		e_GameState29 = GameState29.WAITING_4_OK2;
		this.signal();
	}
	
	public void UpdateReady2(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		arr_iClientDoubleRedouble[((Integer)msg.arr_data[1]).intValue()] = ((Integer)msg.arr_data[2]).intValue();
		this.signal();
	}

	public void UpdateReady3(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		arr_iClientReady3[((Integer)msg.arr_data[1]).intValue()] = ((Integer)msg.arr_data[2]).intValue();
		if(((Integer)msg.arr_data[2]).intValue() == 1){
			ListIterator<CCard29> it = list_mycards.listIterator();
			while(it.hasNext()){
				CCard29 card = it.next();
				if(card.IsSeventh()){
					card.SetSeventh(false);
					break;
				}
			}	
			Collections.sort(list_mycards);
		}
		this.signal();
	}
	
	public void UpdateStartPlay(CMessage29 msg){
		e_GameState29 = GameState29.PLAYING;
		for(int i=0; i<4; i++){
			arr_lastTable[i] = arr_currentTable[i];
			arr_currentTable[i] = null;
		}
		this.i_lasthandWinner = this.i_HandWinner;
		this.i_HandWinner = -1;
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		this.signal();
	}

	public void UpdatePlayedCard(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		arr_currentTable[((Integer)msg.arr_data[1]).intValue()] = (CCard29) msg.arr_data[2];
		if(((Integer)msg.arr_data[1]).intValue() == this.GetClientId()){
			list_mycards.remove(msg.arr_data[2]);
		}
		this.signal();
	}
	
	public void UpdateHandWinnerInfo(CMessage29 msg){
		e_GameState29 = GameState29.PLAY_PAUSED;
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		this.i_HandWinner = ((Integer)msg.arr_data[1]).intValue();
		arr_iHandWonCnt[i_HandWinner]++;
		this.signal();
	}
	
	public void UpdateBadPlayInfo(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		this.signal(); //Don't process badplay msg just signal so that player can play again
	}
	
	public void UpdatePlayedShowTrump(CMessage29 msg){
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		o_trump = (CCard29)msg.arr_data[2];
		if(this.b_isSeventh && (this.GetClientId() == this.i_BidWinnerId)){
			ListIterator<CCard29> it = list_mycards.listIterator();
			while(it.hasNext()){
				CCard29 card = it.next();
				if(card.IsSeventh()){
					card.SetSeventh(false);
					break;
				}
			}
			Collections.sort(list_mycards);
		}
			
		this.signal();
	}
	
	public void UpdateGameCompletionInfo(CMessage29 msg){
		e_GameState29 = GameState29.PLAY_FIN;
		for(int i=0; i<4; i++){
			arr_lastTable[i] = arr_currentTable[i];
			arr_currentTable[i] = null;
			SetReadyNess(i, false);
		}
		this.i_lasthandWinner = this.i_HandWinner;
		this.i_HandWinner = -1;
		arr_nextjob = ((NextJobTypes[])msg.arr_data[0]);
		o_gameCompletionInfo = (CGameCompletionInfo) msg.arr_data[1];
		o_gamepoint = (CGamePointInfo) msg.arr_data[2];
		this.signal();
	}
	
	public void UpdateShowPair(CMessage29 msg){
		this.i_PairShowClientId = ((Integer)msg.arr_data[1]).intValue();
		this.i_bidChange = ((Integer)msg.arr_data[2]).intValue();
		this.signal();
	}
}
