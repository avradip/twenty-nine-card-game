package client;

import common.*;


public class C4PlayerGameClient extends CClient {
	
//	private Object syncobject;
//	private boolean b_seatalloc_rcvd;

	
	public C4PlayerGameClient(String name, String serverip, int port,boolean isChild){
		super(name,serverip,port,4,true);
		if(!isChild)
			this.o_clientdata = new C4PlayerGameClientData();
	}
	
	@Override
	protected void ProcessMessage(Object obj){
		try{
			CMessage4Player msg = (CMessage4Player)obj;
			System.out.println("C4PlayerGameClient:ProcessMessage. MsgId " + msg.l_msgid);	
			
			switch(msg.e_datatype4player){
				case CURRENT_SEAT_ALLOC:
					((C4PlayerGameClientData)o_clientdata).UpdateSeatList(msg);
					break;
				case CURRENT_READY_STATUS:
					((C4PlayerGameClientData)o_clientdata).UpdateReadyNess(msg);
			}
		}
		catch(ClassCastException e){
			super.ProcessMessage(obj);
		}
	}
	
	
	public  void SendSeatReq(int i){
		if(o_clientdata.GetClientId()==-1){
			System.err.println("C4PlayerGameClient::SendSeatReq::Haven't yet received client id, won't send seat request");
			return;
		}
		System.out.println("C4PlayerGameClient::SendSeatReq::user requested Seat No. " + i);	
		CMessage4Player sndmsg = CMessage4Player.CreateMsg_BlockSeatReq_Client(o_clientdata.GetClientId(), this.i_outmsg_cnt++, i);
		this.q_msg_out.add(sndmsg);
	}
	
	public void SendReady(){
		CMessage4Player msg = CMessage4Player.CreateMsgSendReady_Client(o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		q_msg_out.add(msg);
	}
	
	public void Init() throws InterruptedException{
		System.out.println("C4PlayerGameClient::C4PlayerGameClient::Init()");	
		super.Init();
		
		CMessage4Player msg = CMessage4Player.CreateMsg_GetSeatAlloc_Client(o_clientdata.GetClientId(), this.i_outmsg_cnt++);
		this.q_msg_out.add(msg);
//		if(!b_seatalloc_rcvd){
//			synchronized(syncobject){
//				syncobject.wait();
//			}
//		}
	}
}
