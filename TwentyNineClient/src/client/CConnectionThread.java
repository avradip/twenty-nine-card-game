package client;

import java.io.*;
import java.net.*;
import common.*;
import common.CMessage.DataType;
import common.CMessage.MsgType;

public class CConnectionThread extends Thread {
	private CClient o_client;
	private boolean b_connecting = false;
	private Socket 	socket;
	public CConnectionThread(CClient client){
		o_client = client;
	}
	public void abort(){
		b_connecting = false;
		System.out.println("CConnectionThread::abort()");
	}
	public void run(){
		b_connecting = true;
		System.out.println("CCOnnectionThread::Starting Thread");
        try {
        	while(b_connecting){
	            socket = new Socket(o_client.GetServerip(), o_client.GetServerport());
	            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
	            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
	            CMessage msgsnd;
	            if(o_client.o_clientdata.GetClientId() == -1){
	            	msgsnd = new CMessage(MsgType.HELLO);
	            	msgsnd.e_datatype = DataType.CLIENT_NAME;
	            	msgsnd.arr_data = new String[1];
	            	msgsnd.arr_data[0] = o_client.GetName();
	            }
	            else{
	            	if(o_client.q_msg_out.isEmpty()){
		            	msgsnd = new CMessage(MsgType.GETNEXTMSG);
		            	msgsnd.i_clientid = o_client.o_clientdata.GetClientId();
	            	}
	            	else
	            		msgsnd = o_client.q_msg_out.poll();
	            }
	            out.writeObject(msgsnd);
	            CMessage msgrcv;
	            msgrcv = (CMessage)in.readObject();
	            if(msgrcv.e_type == MsgType.SERVERFULL){
	            	System.err.println("Serverfull");
	            	o_client.o_clientdata.SetClientId(-1);
	            	break;
	            }
	            if(msgrcv.e_type == MsgType.WELCOME){
	            	o_client.o_clientdata.SetClientId(msgrcv.i_clientid);
//	            	synchronized(o_client.syncobject){
//	            		System.out.println("CConnectionThread: Going to notify Init thread, Clientid has been updated. id " + o_client.o_clientdata.GetClientId());
//	            		o_client.syncobject.notify();
//	            	}
	            	System.out.println("Connection Successfull with server " + o_client.GetServerip()+ " Received clientid " + o_client.o_clientdata.GetClientId()); 
	            }
	            if(msgrcv.e_type == MsgType.NOMSGAVL){
	            	//System.out.println("No more msg available");
	            }
	            if(msgrcv.e_type == MsgType.ACK_RCV){
	            	System.out.println("Message sent acknwoledged by server. Msg id " + msgrcv.l_msgid);
	            }	            
	            if(msgrcv.e_type == MsgType.SERVMSG){
	            	synchronized(o_client.q_msg_in){
	            		o_client.q_msg_in.add(msgrcv);
	            		o_client.q_msg_in.notify();
	            	}
	            	System.out.println("Message received from server. Msg id " + msgrcv.l_msgid);
	            }
	            out.close();
	            in.close();
	            socket.close();
	            Thread.sleep(1000L);
        	}
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + o_client.GetServerip());
            o_client.o_clientdata.SetClientId(-1);
            synchronized(o_client.q_msg_in){
            	o_client.q_msg_in.notify();
            }
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: " + o_client.GetServerip());
            o_client.o_clientdata.SetClientId(-1);
            synchronized(o_client.q_msg_in){
            	o_client.q_msg_in.notify();
            }
        } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
