package client;

import common.CMessage4Player;

public class C4PlayerGameClientData extends CClientData {

	private int arr_iSeatList[]; //if arr_iSeatList[i] = j, that would mean i^th seat is taken by j^th Player
	private boolean arr_bReadyList[];
	public boolean GetReadyNess(int iPlayerid){ 
		return arr_bReadyList[iPlayerid];
	}
	protected void SetReadyNess(int iPlayerid, boolean b_readyNess){
		arr_bReadyList[iPlayerid] = b_readyNess;
	}
	public int GetIdOnSeat(int iSeatno){return arr_iSeatList[iSeatno];} 
	public int GetSeatForId(int iPlayerId){
		for(int i=0; i<i_totplayer; i++){
			if(arr_iSeatList[i] == iPlayerId)
				return i;
		}
		return -1;
	}
	
	C4PlayerGameClientData(){
		super(4);
		arr_iSeatList = new int[i_totplayer];
		arr_bReadyList = new boolean[i_totplayer];
		for(int i = 0; i<i_totplayer; i++){
			arr_iSeatList[i] = -1;
			arr_bReadyList[i] = false;
		}
	}
	
//	public void AssignSeat(int seatno, int playerid){
//		arr_iSeatList[seatno] = playerid;
//		this.signal();
//	}
	
	public void UpdateSeatList(CMessage4Player msg){
		for(int i = 0; i <i_totplayer; i++)
			arr_iSeatList[i] = ((Integer)msg.arr_data[i]).intValue();
		this.signal();
		CMessage4Player.PrintSeatAlloc(msg);
	}
	
	public void UpdateReadyNess(CMessage4Player msg){
		for(int i = 0; i <i_totplayer; i++)
			arr_bReadyList[i] = ((Boolean)msg.arr_data[i]).booleanValue();
		this.signal();
	}
	
}
