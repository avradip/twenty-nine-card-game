package server;

import java.util.LinkedList;

import common.CCard29;
import common.CMessage29.NextJobTypes;

public class C29ClientS extends C4PlayerClientS {
	private LinkedList<CCard29> list_cardsCurrent; 
	private LinkedList<CCard29> list_cardsWon;
	private int i_Ready2Type; public int GetReady2Type(){ return i_Ready2Type;} public void SetReady2Type(int iReady){ i_Ready2Type = iReady;}
	private int i_SingleHand; public int GetReady3Type(){ return i_SingleHand;} public void SetReady3Type(int iSCall){ i_SingleHand = iSCall;}
	
	C29ClientS(){
		list_cardsCurrent = new LinkedList<CCard29>();
		list_cardsWon = new LinkedList<CCard29>(); 
	}
	public LinkedList<CCard29> GetCurrentCardList(){
		return list_cardsCurrent;
	}
	
	public LinkedList<CCard29> GetWonCardList(){
		return list_cardsWon;
	}
	/**
	 * @return the nextjob
	 */
	public NextJobTypes getNextjob() {
		return nextjob;
	}
	/**
	 * @param nextjob the nextjob to set
	 */
	public void setNextjob(NextJobTypes nextjob) {
		this.nextjob = nextjob;
	}
	private NextJobTypes nextjob;
}
