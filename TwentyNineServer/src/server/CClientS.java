package server;
import common.*;
import java.util.*;

//This class represents each client in the server side
public class CClientS {
	private int i_id;	public int Getid(){return i_id;}	public void Setid(int i){i_id=i;}
	private int i_state;	public int Getstate(){return i_state;}	public void Setstate(int s){i_state = s;}
	//private int i_partnerid; public int Getpartnerid(){return i_partnerid;} public void Setpartnerid(int i){i_partnerid = i;}
	private String s_name; public String GetName(){return s_name;} public void SetName(String name){s_name = name;}
	private String s_ip; public String GetIP(){return s_ip;} public void SetIP(String ip){s_ip = ip;}
	public Queue <CMessage> q_msg_out;
	public Queue <CMessage> q_msg_in;
	public int i_outmsg_cnt;
	public int i_inmsg_cnt;
	public CClientS()
	{
		i_id = -1;
		i_outmsg_cnt = 0;
		i_inmsg_cnt = 0;
		q_msg_in 	= new LinkedList<CMessage>();
		q_msg_out 	= new LinkedList<CMessage>();
	}
}
