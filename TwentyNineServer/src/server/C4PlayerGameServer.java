package server;

import common.*;
import common.CMessage.MsgType;


public class C4PlayerGameServer extends CServer {
	private boolean b_everybodyready;
	protected int[] arr_iSeatList;
	public C4PlayerGameServer(int port, boolean b_IsChild){
		super(port,4,true);
		arr_iSeatList = new int[i_totplayer];
		for(int i=0; i<i_totplayer; i++)
			arr_iSeatList[i] = -1;
		b_everybodyready = false;
		arr_clientS = new CClientS[i_totplayer];
		if(!b_IsChild)
			for(int i=0; i<i_totplayer; i++)
				arr_clientS[i] = new C4PlayerClientS();

		
	}
	private void Reset(){
		b_everybodyready = false;
		for(int i=0; i<i_totplayer; i++)
			((C4PlayerClientS)arr_clientS[i]).bIsReady = false;
	}
	@Override
	protected void ProcessMsg(Object obj){
		try{
			CMessage4Player msg = (CMessage4Player)obj;
			if(msg.e_type!=MsgType.CLIENTMSG){
				System.err.println("Not valid Message Type ");
				return;
			}
			
			//CMessage4Player msg4player = (CMessage4Player)msg;
			switch(msg.e_datatype4player){
			
				case BLOCK_SEAT_REQ:{
					int clientid = msg.i_clientid;
					if(((C4PlayerClientS)arr_clientS[clientid]).bIsReady)
						break;
					int seatreq = ((Integer)msg.arr_data[0]).intValue();
					if(((C4PlayerClientS)arr_clientS[clientid]).iSeatNo != -1){
						arr_iSeatList[((C4PlayerClientS)arr_clientS[clientid]).iSeatNo] = -1;
						((C4PlayerClientS)arr_clientS[clientid]).iSeatNo = -1;
					}
					if(arr_iSeatList[seatreq]==-1){
						arr_iSeatList[seatreq] = clientid;
						((C4PlayerClientS)arr_clientS[clientid]).iSeatNo = seatreq;
					}
					
					CMessage4Player sndmsg;						
					sndmsg = CMessage4Player.CreateMsg_CurrentSeatAlloc_Serv(-1, -1, arr_iSeatList);
					this.BroadCast(sndmsg);				
					boolean arr_bReady[] = new boolean[i_totplayer];
					for(int i = 0; i<i_totplayer; i++)
						arr_bReady[i] = ((C4PlayerClientS)arr_clientS[i]).bIsReady;
					sndmsg = CMessage4Player.CreateMsg_CurrentReadyStatus_Serv(-1, -1, arr_bReady);
					this.BroadCast(sndmsg);
					break;
				}
			
				case GET_SEAT_ALLOC :{
					int clientid = msg.i_clientid;
					CMessage4Player sndmsg;
					sndmsg = CMessage4Player.CreateMsg_CurrentSeatAlloc_Serv(clientid, GetClients()[clientid].i_outmsg_cnt++, arr_iSeatList);
					GetClients()[clientid].q_msg_out.add(sndmsg);
					boolean arr_bReady[] = new boolean[i_totplayer];
					for(int i = 0; i<i_totplayer; i++)
						arr_bReady[i] = ((C4PlayerClientS)arr_clientS[i]).bIsReady;
					sndmsg = CMessage4Player.CreateMsg_CurrentReadyStatus_Serv(clientid, GetClients()[clientid].i_outmsg_cnt++, arr_bReady);
					GetClients()[clientid].q_msg_out.add(sndmsg);
					break;

				}
				
				case READY: {
					int clientid = msg.i_clientid;
					if(((C4PlayerClientS)arr_clientS[clientid]).iSeatNo == -1)
						break;
					((C4PlayerClientS)arr_clientS[clientid]).bIsReady = true;
					CMessage4Player sndmsg;
					boolean arr_bReady[] = new boolean[i_totplayer];
					for(int i = 0; i<i_totplayer; i++)
						arr_bReady[i] = ((C4PlayerClientS)arr_clientS[i]).bIsReady;
					sndmsg = CMessage4Player.CreateMsg_CurrentReadyStatus_Serv(-1, -1, arr_bReady);
					this.BroadCast(sndmsg);
					SomeOneGotReady();
					
				}
			}
		}
		catch(ClassCastException e){
			super.ProcessMsg(obj);
			return;
		}
	}
	
	
	private void SomeOneGotReady() {
		for(int i= 0 ; i<i_totplayer; i++)
			if(!((C4PlayerClientS)arr_clientS[i]).bIsReady)
				return;
		b_everybodyready = true;
		synchronized(this){
			this.notifyAll();
		}
	}



	protected void WaitForEverybodyTobeReady() throws InterruptedException{
		Reset();
		while(!this.b_everybodyready && b_running){
			synchronized(this){
				this.wait();
			}
		}
		return;
	}
	
	@Override
	public void Init() throws InterruptedException{
		super.Init();		
	}
}
