package gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import server.C29Server;
import org.eclipse.swt.layout.GridLayout;

import bot.CBotBasic;

public class FormMainServer {

	protected Display display;
	protected Shell shlPlay;
	private Text textPort;
	private Button btnStart;
	private Button btnStop;
	private Group grpBots;
	private Button arr_checkBotbtns[];
	String sDefaultPort = "6666";
	C29Server o_server;
	CBotBasic arr_bots[];
	/**
	 * Launch the application.
	 * @param args
	 */
	public FormMainServer(){
		o_server = null;
		arr_bots = new CBotBasic[4];
		arr_checkBotbtns = new Button[4];
	}
	public static void main(String[] args) {
		try {
			FormMainServer window = new FormMainServer();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		shlPlay = new Shell(display,SWT.CLOSE | SWT.TITLE | SWT.MIN);
		shlPlay.setLayout(new FillLayout());
		
		Group group = new Group(shlPlay, SWT.NONE);
		group.setLayout(new FormLayout());
		
		Label lblPort = new Label(group, SWT.BORDER);
		lblPort.setAlignment(SWT.CENTER);
		FormData fd_lblPort = new FormData();
		fd_lblPort.bottom = new FormAttachment(15, 10);
		fd_lblPort.top = new FormAttachment(0, 10);
		fd_lblPort.right = new FormAttachment(30, 20);
		fd_lblPort.left = new FormAttachment(0, 20);
		lblPort.setLayoutData(fd_lblPort);
		lblPort.setText("Port");
		
		textPort = new Text(group, SWT.BORDER);
		FormData fd_textPort = new FormData();
		fd_textPort.bottom = new FormAttachment(lblPort, 0, SWT.BOTTOM);
		fd_textPort.top = new FormAttachment(lblPort, 0, SWT.TOP);
		fd_textPort.right = new FormAttachment(100, -20);
		fd_textPort.left = new FormAttachment(70, -20);
		textPort.setLayoutData(fd_textPort);
		
		btnStart = new Button(group, SWT.NONE);
		btnStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StartClick();
			}
		});
		FormData fd_btnStart = new FormData();
		fd_btnStart.right = new FormAttachment(lblPort, 0, SWT.RIGHT);
		fd_btnStart.left = new FormAttachment(lblPort, 0, SWT.LEFT);
		fd_btnStart.top = new FormAttachment(85, -10);
		fd_btnStart.bottom = new FormAttachment(100, -10);
		btnStart.setLayoutData(fd_btnStart);
		btnStart.setText("Start");
		
		btnStop = new Button(group, SWT.NONE);
		btnStop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StopClick();
			}
		});
		FormData fd_btnStop = new FormData();
		fd_btnStop.bottom = new FormAttachment(btnStart, 0, SWT.BOTTOM);
		fd_btnStop.top = new FormAttachment(btnStart, 0, SWT.TOP);
		fd_btnStop.left = new FormAttachment(textPort, 0, SWT.LEFT);
		fd_btnStop.right = new FormAttachment(textPort, 0, SWT.RIGHT);
		btnStop.setLayoutData(fd_btnStop);
		btnStop.setText("Stop");
		
		grpBots = new Group(group, SWT.NONE);
		grpBots.setText("Bots");
		grpBots.setLayout(new GridLayout(2, false));
		FormData fd_grpBots = new FormData();
		fd_grpBots.bottom = new FormAttachment(btnStart, -5);
		fd_grpBots.top = new FormAttachment(lblPort, 5);
		fd_grpBots.right = new FormAttachment(textPort, 0, SWT.RIGHT);
		fd_grpBots.left = new FormAttachment(lblPort, 0, SWT.LEFT);
		grpBots.setLayoutData(fd_grpBots);		

		createContents();
		ContentCreationDone();
		shlPlay.open();
		shlPlay.layout();
		while (!shlPlay.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		for(int i=0; i<4; i++){
			arr_checkBotbtns[i] = new Button(grpBots, SWT.CHECK);
			arr_checkBotbtns[i].setText("Seat"+i);
		}
		shlPlay.setSize(191, 191);
		shlPlay.setText("29 Server");
	}
	
	private void ContentCreationDone(){
		this.textPort.setText(this.sDefaultPort);
		
		this.btnStop.setEnabled(false);
		shlPlay.addListener (SWT.Close, new Listener(){
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				CloseApp();
			}
		});
	}
	
	private void CloseApp(){
		if(o_server!=null){
			o_server.abort();
			o_server = null;
		}
	}
	private void StartClick(){
		if(o_server==null){
			o_server = new C29Server(Integer.parseInt(textPort.getText()));
			o_server.start();
			synchronized(o_server){
				try {
					o_server.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			for(int i=0; i<4; i++){
				if(this.arr_checkBotbtns[i].getSelection()){
					arr_bots[i] = new CBotBasic("localhost",o_server.GetPort(),i);
					arr_bots[i].start();
				}					
			}
		}
		this.grpBots.setEnabled(false);
		this.btnStart.setEnabled(false);
		this.btnStop.setEnabled(true);
	}
	
	private void StopClick(){
		if(o_server!=null){
			o_server.abort();
			o_server = null;
		}
		for(int i=0; i<4; i++){
			if(arr_bots[i]!=null){
				arr_bots[i].abort();
				arr_bots[i] = null;
			}
		}
		this.grpBots.setEnabled(true);
		this.btnStart.setEnabled(true);
		this.btnStop.setEnabled(false);
	}
}
