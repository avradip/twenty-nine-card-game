package common;


public class CMessage29 extends CMessage4Player {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;

	//Server Message:
	// 	arr_data is an array of Three Objects
	// 	arr_data[0] is an array of Length 4, denoting NextJobTypes for each client (ordered with respect to client id)
	// 	arr_data[1] type is denoted by the field e_datatype29:
	//		FOUR_CARDS: arr_data[1] is an array of 4 CCard29
	//		Four_CARDS2:arr_data[1] is an array of 4 CCard29
	//		JOB_DONE_BID: arr_data[1] is an array of two objects
	//			arr_data[1][0] denotes the clientid of the client which completed the job
	//			arr_data[1][1] denotes the Integer Bid
	//		JOB_NOT_DONE: arr_data[1] is an array of two objects
	//			arr_data[1][0] denotes the clientid of the client which committed an non acceptable job
	//			arr_data[1][1] is of NextJobTypes denoting the job type he tried to do
	//		BID_COMPLETION_INFO: Integer Type, Bid Winner Client Id
	//		JOB_DONE_SET_TRUMP: Integer Type, Bid Winner Client Id
	//		JOB_DONE_GET_READY2: Integer Type, Client id for the player who is ready for next set of cards
	//		Four_CARDS2: arr_data[1] is an array of 4 CCard29 (this is the second set of dealt cards)
	//		JOB_DONE_GET_READY3: Integer Type clientid of the player who got ready3
	//		JOB_DONE_PLAY_CARD:		Integer Type clientid of the player who played
	//		JOB_DONE_PLAY_SHOWTRUMP:	Integer Type clientid of the player who played
	//		JOB_DONE_PLAY_PAIR:		 	Integer Type clientid of the player who played
	//		START_PLAY:					null
	//		HAND_WINNER_INFO:			Integer Type, clientid of the player who won
	//		JOB_DONE_BAD_PLAY			Integer Type, clientid of the player who made invalid pay
	//		GAME_COMPLETION_INFO:		CGameCompletionInfo Type,
	
	//	arr_data[2] is Object type
	//		FOUR_CARDS: Integer denoting next min bid, in other words it should be 16
	//		Four_CARDS2: null		
	//		JOB_DONE_BID: Integer denoting next min bid, if there is no next bid then it's -1(comment do we actually use -1 anywhere??)
	//		BID_COMPLETION_INFO: 	Integer type, Winning Bid
	//		JOB_DONE_SET_TRUMP:		Boolean Type, Denoting whether Trump is Seventh Card
	//		JOB_DONE_GET_READY2:	Integer Type, 0 for Ready, 1 for Ready and Double, 2 for Ready and Redouble
	//		JOB_DONE_GET_READY3: 	Integer Type, denoting Ready3 type, 0 means ready, 1 means singlehand call
	//		JOB_DONE_PLAY_CARD:		CCard29 Type, the played card
	//		JOB_DONE_PLAY_SHOWTRUMP:	CCard29 Type, the trump card... some card with same suit as Trump, in case of seventh card actual trumpcard, in case of notrump some card with suit 4
	//		JOB_DONE_PLAY_PAIR:			Integer Type, the change in bid because of shown pair, 0, -4, or 4
	//		START_PLAY:					null
	//		HAND_WINNER_INFO: 			null
	//		JOB_DONE_BAD_PLAY:			null
	//		GAME_COMPLETION_INFO:		CGamePointInfo Type

	//Client Message:
	//	depending on e_datatype29,
	//	JOB_DONE_BID: arr_data is an array of one Object, i.e. arr_data[0] is an Integer with the bid value, bid value 0 would denote pass
	//	JOB_DONE_SET_TRUMP: arr_data is an array of one Object, i.e. arr_data[0] is an Integer with the trump suit. 0-3 = HCDS, 4=No Trump, 5 = Seventh.
	//	JOB_DONE_GET_READY2: arr_data is an array of one Object, i.e. arr_data[0] is an Integer Type, 0 for Ready, 1 for Ready and Double, 2 for Ready and Redouble
	// 	JOB_DONE_GET_READY3: arr_data is an array of one Object, i.e. arr_data[0] is an Integer Type, 0 for Ready, 1 for SingleHand Call
	// 	JOB_DONE_PLAY_CARD:	 arr_data is an array of one Object, i.e. arr_data[0] is an CCard29 Type
	//	JOB_DONE_PLAY_SHOWTRUMP:	arr_data is null
	//	JOB_DONE_PLAY_PAIR:			arr_data is null


	
	public CMessage29(MsgType type) {
		super(type);
		// TODO Auto-generated constructor stub
	}
	
	public enum NextJobTypes{
		WAIT,
		BID,
		SET_TRUMP,
		GET_READY2, //asking whether ready for second set of cards or opting for double/redouble
		GET_READY3, //asking whether ready to start play or opting for single hand
		PLAY
	}
	
	public enum DataType29{
		FOUR_CARDS, 			//Server type
		FOUR_CARDS2, 			//Server type		
		BID_COMPLETION_INFO, 	//Server type
		START_PLAY,				//Server type
		JOB_DONE_BAD_PLAY,		//Server type
		HAND_WINNER_INFO,		//Server type
		GAME_COMPLETION_INFO,	//Server type
		JOB_DONE_BID, 			//Client and Server type
		JOB_DONE_SET_TRUMP, 	//Client and Server Type
		JOB_DONE_GET_READY2, 	//Client and Server Type
		JOB_DONE_GET_READY3, 	//Client and Server Type		
		JOB_DONE_PLAY_CARD,			//Client and Server Type
		JOB_DONE_PLAY_SHOWTRUMP, 	//Client and Server Type
		JOB_DONE_PLAY_PAIR			//Client and Server Type
	}

	//This Enum Do not really belong over in this Class, but I am too lazy to make a new file for this tiny class
	public enum GameState29{//This enum list should be in exact order as the game proceeds
		UNDEFINED,
		BIDDING,
		SETTING_TRUMP,
		WAITING_4_OK2,	//waiting for ok from everybody so that next 4 cards can be dealt
		WAITING_4_OK3,	//waiting for ok from everybody so that play can be started
		PLAYING,
		PLAY_PAUSED,
		PLAY_FIN
	}
	

	public static CMessage29 CreateMsg_GameCompletionInfo_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs,
			CGameCompletionInfo o_gameCompleInfo, CGamePointInfo o_gamepoint){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.GAME_COMPLETION_INFO;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = o_gameCompleInfo;
		snd.arr_data[2] = o_gamepoint;
		return snd;
	}
	
	
	public static CMessage29 CreateMsg_JobDoneBadPlay_Serv(int i_clientid, int i_msgid, int i_badplayerid, NextJobTypes[] e_nextjobs){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_BAD_PLAY;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_badplayerid);
		snd.arr_data[2] = null;
		return snd;
	}
	public static CMessage29 CreateMsg_HandWinnerInfo_Serv(int i_clientid, int i_msgid, int i_winnerId, NextJobTypes[] e_nextjobs){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.HAND_WINNER_INFO;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_winnerId);
		snd.arr_data[2] = null;
		return snd;
	}
	public static CMessage29 CreateMsg_StartPlay_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.START_PLAY;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = null;
		snd.arr_data[2] = null;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDonePlayCard_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int i_clientPlayed, CCard29 card){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_PLAY_CARD;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_clientPlayed);
		snd.arr_data[2] = card;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDonePlayShowTrump_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int i_clientPlayed, CCard29 trumpcard){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_PLAY_SHOWTRUMP;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_clientPlayed);
		snd.arr_data[2] = trumpcard;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDonePlayCard_Client(int i_clientid, int i_msgid, CCard29 card){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_PLAY_CARD;
		snd.arr_data = new Object[1];
		snd.arr_data[0] = card;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDonePlayShowTrump_Client(int i_clientid, int i_msgid){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_PLAY_SHOWTRUMP;
		snd.arr_data = null;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDonePlayPair_Client(int i_clientid, int i_msgid){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_PLAY_PAIR;
		snd.arr_data = null;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDonePair_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int i_clientPlayed, int i_bidChange){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_PLAY_PAIR;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_clientPlayed);
		snd.arr_data[2] = new Integer(i_bidChange);
		return snd;
	}
	
	public static CMessage29 CreateMsg_FourCards2_Serv(CCard29[] arr_cards, int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = arr_cards;
		snd.e_datatype29 = DataType29.FOUR_CARDS2;
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDoneGetReady3_Client(int iReadyNess, int i_clientid, int i_msgid){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_GET_READY3;
		snd.arr_data = new Object[1];
		snd.arr_data[0] = new Integer(iReadyNess);
		return snd;
	}

	public static CMessage29 CreateMsg_JobDoneGetReady3_Serve(int i_clientid, int i_msgid,NextJobTypes[] e_nextjobs, int i_readyclientid, int i_readyness){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_GET_READY3;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_readyclientid);
		snd.arr_data[2] = new Integer(i_readyness);
		return snd;
	}
	
	public static CMessage29 CreateMsg_JobDoneGetReady2_Client(int iReadyNess, int i_clientid, int i_msgid){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_GET_READY2;
		snd.arr_data = new Object[1];
		snd.arr_data[0] = new Integer(iReadyNess);
		return snd;
	}

	public static CMessage29 CreateMsg_JobDoneGetReady2_Serve(int i_clientid, int i_msgid,NextJobTypes[] e_nextjobs, int i_readyclientid, int i_readyness){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.e_datatype29 = DataType29.JOB_DONE_GET_READY2;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_readyclientid);
		snd.arr_data[2] = new Integer(i_readyness);
		return snd;
	}
	
	public static CMessage29 CreateMsg_SetTrump_Client(int iSuit, int i_clientid, int i_msgid){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[1];
		snd.arr_data[0] = new Integer(iSuit);
		snd.e_datatype29 = DataType29.JOB_DONE_SET_TRUMP;
		return snd;
	}
	
	public static CMessage29 CreateMsg_TrumpSetDone_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int i_bidwinner, boolean b_isSeventh){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(i_bidwinner);
		snd.arr_data[2] = new Boolean(b_isSeventh);
		snd.e_datatype29 = DataType29.JOB_DONE_SET_TRUMP;
		return snd;
	}
	public DataType29 e_datatype29;

	public static CMessage29 CeateMsg_Bid_Client(int iBid, int i_clientid, int i_msgid){
		CMessage29 snd = new CMessage29(MsgType.CLIENTMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[1];
		snd.arr_data[0] = new Integer(iBid);
		snd.e_datatype29 = DataType29.JOB_DONE_BID;
		return snd;
	}
	public static CMessage29 CreateMsg_Deal4Cards_Serv(CCard29[] arr_cards, int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int iNextMinBid){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = arr_cards;
		snd.arr_data[2] = new Integer(iNextMinBid);
		snd.e_datatype29 = DataType29.FOUR_CARDS;
		return snd;
	}
	public static CMessage29 CreateMsg_BidUpdated_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int iBidClientId, int iCurrentBid, int iNextMinBid){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Object[2];
		((Object[])snd.arr_data[1])[0] = new Integer(iBidClientId);
		((Object[])snd.arr_data[1])[1] = new Integer(iCurrentBid);
		snd.arr_data[2] = new Integer(iNextMinBid);
		snd.e_datatype29 = DataType29.JOB_DONE_BID;
		return snd;
	}
	public static CMessage29 CreateMsg_BidCompletionInfo_Serv(int i_clientid, int i_msgid, NextJobTypes[] e_nextjobs, int iBidWinnerId, int iWinningBid){
		CMessage29 snd = new CMessage29(MsgType.SERVMSG);
		snd.i_clientid = i_clientid;
		snd.l_msgid = i_msgid;
		snd.arr_data = new Object[3];
		snd.arr_data[0] = e_nextjobs;
		snd.arr_data[1] = new Integer(iBidWinnerId);
		snd.arr_data[2] = new Integer(iWinningBid);
		snd.e_datatype29 = DataType29.BID_COMPLETION_INFO;
		return snd;
	}

}
