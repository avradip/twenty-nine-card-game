package common;

public class CCard29 extends CCard {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6L;
	private  int point; //J = 3, 9=2, A=1, 10=1 everything else 0
	private  int order; //Order of the value to win, higher the better
	private boolean b_isSeventh;
	public boolean IsSeventh(){return b_isSeventh;} public void SetSeventh(boolean bSeventh){b_isSeventh = bSeventh;}
	public 	 int GetPoint(){return point;}
	public   int GetOrder(){return order;}
	public CCard29(int s, int v){
		  super(s,v);
		  b_isSeventh = false;
		  point = order = 0;
		  switch(GetValue()){
		  case 11:
		    point = 3;
		    order =13;
		    break;
		  case 9:
		    point = 2;
		    order = 12;
		    break;
		  case 1:
		    point = 1;
		    order = 11;
		    break;
		  case 10:
		    point = 1;
		    order = 10;
		    break;
		  case 13:
		    order = 9;
		    break;
		  case 12:
		    order = 8;
		    break;
		  case 8:
		    order = 7;
		    break;
		  case 7:
		    order = 6;
		    break;
		  default:
		    order = 0;
		  }
		}
	@Override
	public int compareTo(CCard o) {
		// TODO Auto-generated method stub
		CCard29 o29;
		o29 = (CCard29)o;
		if(this.IsSeventh() && !o29.IsSeventh())
			return 1;
		if(!this.IsSeventh() && o29.IsSeventh())
			return -1;
		if(this.GetSuit() > o29.GetSuit())
			return 1;
		if(this.GetSuit() < o29.GetSuit())
			return -1;
		if(this.GetOrder() < o29.GetOrder())
			return 1;
		if(this.GetOrder() > o29.GetOrder())
			return -1;
		return 0;
	}
	
	@Override
	public boolean equals(Object o){
		if(compareTo((CCard29)o)==0)
			return true;
		else
			return false;
	}
}
