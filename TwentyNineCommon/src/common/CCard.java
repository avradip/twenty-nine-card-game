package common;

import java.io.Serializable;

public class CCard extends Object implements Serializable , Comparable<CCard>{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 5L;
	private int suit; //Suit Hearts=0, Club=1, Diamond=2, Spread=3
	 private  int value; //1=Ace, 2-10, 11=J, 12=Q, 13=K

	 public	  CCard(int s, int v){suit=s; value=v;}
	 public int GetSuit(){return suit;}
	 public int GetValue(){return value;}
	 public String toString(){
		 String s_suit;
		 String s_value;
		 switch(suit){
		 case 1: 
			 s_suit = "\u2663";break;
		 case 2:
			 s_suit = "\u2666"; break;
		 case 0:
			 s_suit = "\u2665"; break;
		 case 3:
			 s_suit = "\u2660"; break;
		 default:
			 s_suit = "Error ";
		 }
		 switch(value){
		 case 1:
			 s_value = "A"; break;
		 case 11:
			 s_value = "J"; break;
		 case 12:
			 s_value = "Q"; break;
		 case 13:
			 s_value = "K"; break;
		 default:
			 s_value = Integer.toString(value);
		 }
		 return (s_value + s_suit);
	 }
	@Override
	public int compareTo(CCard o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
