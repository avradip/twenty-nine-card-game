package common;


public class CMessage4Player extends CMessage {

	
	public enum DataType4Player{
		GET_SEAT_ALLOC, //arr_data is null, For Msg Sent by Client
		BLOCK_SEAT_REQ, //arr_data is an array of Integer of size 1 , For Msg Sent by Client
		CURRENT_SEAT_ALLOC, //arr_data an array of Integer of size 4, containing current seat allocation. For msg Sent by Server
		CURRENT_READY_STATUS, //arr_data an array of booleans of size 4, containing current readyness info. For msg Sent by Server
		READY			//arr_data is null								; This is ClientMsg
		};
	
	private static final long serialVersionUID = 3L;
	public DataType4Player e_datatype4player;
	
	public CMessage4Player(MsgType type) {
		super(type);
		// TODO Auto-generated constructor stub
	}
	
	public static CMessage4Player CreateMsg_GetSeatAlloc_Client(int clientid, int msgid){
		CMessage4Player msg = new CMessage4Player(MsgType.CLIENTMSG);
		msg.e_datatype4player = DataType4Player.GET_SEAT_ALLOC;
		msg.i_clientid = clientid;
		msg.l_msgid = msgid;
		msg.arr_data = null;
		return msg;
	}
	
	public static CMessage4Player CreateMsg_BlockSeatReq_Client(int clientid, int msgid, int seatid){
		CMessage4Player msg = new CMessage4Player(MsgType.CLIENTMSG);
		msg.e_datatype4player = DataType4Player.BLOCK_SEAT_REQ;
		msg.i_clientid = clientid;
		msg.l_msgid = msgid;
		msg.arr_data = new Integer[1];
		msg.arr_data[0] = new Integer(seatid);
		return msg;
	}
	

	public static CMessage4Player CreateMsg_CurrentSeatAlloc_Serv(int clientid, int msgid, int[] arr_iSeatList){
		CMessage4Player msg = new CMessage4Player(MsgType.SERVMSG);
		msg.e_datatype4player = DataType4Player.CURRENT_SEAT_ALLOC;
		msg.i_clientid = clientid;
		msg.l_msgid = msgid;
		msg.arr_data = new Integer[4];
		for(int i=0;i<4;i++)
			msg.arr_data[i] = new Integer(arr_iSeatList[i]);
		return msg;
	}
	
	public static CMessage4Player CreateMsg_CurrentReadyStatus_Serv(int clientid, int msgid, boolean[] arr_bReadyStatus){
		CMessage4Player msg = new CMessage4Player(MsgType.SERVMSG);
		msg.e_datatype4player = DataType4Player.CURRENT_READY_STATUS;
		msg.i_clientid = clientid;
		msg.l_msgid = msgid;
		msg.arr_data = new Boolean[4];
		for(int i=0;i<4;i++)
			msg.arr_data[i] = new Boolean(arr_bReadyStatus[i]);
		return msg;
	}
	
	public static CMessage4Player CreateMsgSendReady_Client(int clientid, int msgid){
		CMessage4Player msg = new CMessage4Player(MsgType.CLIENTMSG);
		msg.e_datatype4player = DataType4Player.READY;
		msg.i_clientid = clientid;
		msg.l_msgid = msgid;
		msg.arr_data = null;
		return msg;
	}
	
	public static void PrintSeatAlloc(CMessage4Player msg){
		System.out.println("Printing Seat Allocation, Msgid " + msg.l_msgid);
		for(int i=0; i<4;i++){
			System.out.println("Seat No - " + i + " .Player Seated " + ((Integer)msg.arr_data[i]).intValue());
		}
	}
}
