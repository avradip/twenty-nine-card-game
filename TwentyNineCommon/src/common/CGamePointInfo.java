package common;

import java.io.Serializable;

public class CGamePointInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8L;

	public int EvenPoint;
	public int EvenSet;
	public int OddPoint;
	public int OddSet;
	public CGamePointInfo(){
		EvenPoint=EvenSet=OddPoint=OddSet=0;
	}
	
	public int UpdatePoint(int i_winnerparity, int i_bidwinnerSeat, int i_doubleRe, boolean b_isSingleHand){
		int i_multiplier;
		switch(i_doubleRe){
		case 1:
			i_multiplier = 2;
			break;
		case 2:
			i_multiplier = 4;
			break;
		default:
			i_multiplier = 1;					
		}
		if(b_isSingleHand)
			i_multiplier = 3;
		
		if(i_winnerparity > 1)
			i_multiplier = Math.max(i_multiplier, 2);
		
		if(i_bidwinnerSeat%2==0){
			if(i_winnerparity%2 == 0)
				EvenPoint+=i_multiplier;
			else
				EvenPoint-=i_multiplier;
		}
		else{
			if(i_winnerparity%2 == 1)
				OddPoint+=i_multiplier;
			else
				OddPoint-=i_multiplier;
		}

		if(EvenPoint >= Math.min(6 - EvenSet, 6)){
			EvenSet+=1;
			OddPoint = 0;
			EvenPoint = 0;
		}
		if(EvenPoint <= Math.max(-6, -6 - EvenSet)){
			EvenSet-=1;
			OddPoint = 0;
			EvenPoint = 0;
		}
		if(OddPoint >= Math.min(6 - OddSet, 6)){
			OddSet+=1;
			OddPoint = 0;
			EvenPoint = 0;
		}
		if(OddPoint <= Math.max(-6, -6 - OddSet)){
			OddSet-=1;
			OddPoint = 0;
			EvenPoint = 0;
		}	
		if(EvenSet >= 5 || OddSet<=-5)
			return 1;
		if(OddSet>=5 || EvenSet <= -5)
			return -1;
		return 0;					
	}
}
