package common;

import java.io.Serializable;
import java.util.LinkedList;

public class CGameCompletionInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7L;
	
	public LinkedList<LinkedList<CCard29>> list_listwincards; //list of lists containing 
	public int arr_points[]; //array of integers denoting points won by each id
	public int winner;		//0/1 value integer, 0 if north-south(seat 0,2) won, 1 otherwise. Also for huge winning, i.e. all hand / can't make half cases, it's 2 or 3
}