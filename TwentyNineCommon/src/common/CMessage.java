package common;

import java.io.Serializable;


public class CMessage extends Object implements Serializable {

	public static class CClientInfo extends Object implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public int i_clientid;
		public String s_clientip;
		public String s_clientname;
	}
	public enum MsgType { HELLO, GETNEXTMSG, BYE, CLIENTMSG,		//Client MsgTypes, only CLIENTMSG types go into server message queue
								WELCOME, SERVERFULL, NOMSGAVL, ACK_RCV, SERVMSG };	//Server MsgTypes, only SERVMSG types go into client message queue 
	public enum DataType { 
		CLIENT_NAME, 			//arr_data is an array (of size one) of strings ; This is ClientMsg
		CONNECTED_CLIENT_INFO, 	//arr_data is an array of CClientInfo objects	; This is ServMsg
		MSG_CHAT				//arr_data is an array (of size one) of strings ; This is both server and client type, 
		};							
	private static final long serialVersionUID = 2L;public static long Getserialid(){return serialVersionUID;}

	public MsgType e_type; 
	public DataType e_datatype;
	public long l_msgid;
	public int i_clientid;
	public Object[] arr_data;
	
	public CMessage(MsgType type){
		e_type = type;
	}
	
	public void printclientinfo(){
		if(e_datatype!=DataType.CONNECTED_CLIENT_INFO)
			System.err.println("Msgtype is not Clientinfo");
		else{
			for(int i=0; i<arr_data.length; i++)
				System.out.println("Clientid: " + ((CClientInfo)arr_data[i]).i_clientid + " ClientIP: " + ((CClientInfo)arr_data[i]).s_clientip + 
						" Client Name: " + ((CClientInfo)arr_data[i]).s_clientname);
		}
	}
	
	public static CMessage CreateMsg_Chat_Serv(int i_clientid, int i_msgid, String sMsg){
		CMessage msg = new CMessage(MsgType.SERVMSG);
		msg.e_datatype = DataType.MSG_CHAT;
		msg.i_clientid = i_clientid;
		msg.l_msgid = i_msgid;
		msg.arr_data = new Object[1];
		msg.arr_data[0] = sMsg;
		return msg;
	}
	
	public static CMessage CreateMsg_Chat_Client(int i_clientid, int i_msgid, String sMsg){
		CMessage msg = new CMessage(MsgType.CLIENTMSG);
		msg.e_datatype = DataType.MSG_CHAT;
		msg.i_clientid = i_clientid;
		msg.l_msgid = i_msgid;
		msg.arr_data = new Object[1];
		msg.arr_data[0] = sMsg;
		return msg;
	}
}
